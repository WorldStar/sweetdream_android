package com.tiseno.sweetdream.sound;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

public class CSound {
	
    public static final String MUSICPATH = "music/";
    
    public static final String SOUNDPATH = "music/";
    
    public static CSound m_instance;
    
    public Music musicTrack;
    
	AssetManager assets;
    SoundPool soundPool;

    HashMap<String, Sound> m_mapSounds;

    
    public CSound(Activity activity) {
    	
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        
        this.soundPool = new SoundPool(30, AudioManager.STREAM_MUSIC, 0);
        
        m_mapSounds = new HashMap<String, Sound>();
    }
    
    public static void initSoundController(Activity activity)
    {
   		m_instance = new CSound(activity);
    }
    
    public static CSound sharedSoundController()
    {
    	return m_instance;	
    }
    
    public Sound loadSoundWithName(String filename)
    {
    	return newSound(filename);
    }
    
    public Music loadMusicWithName(String filename)
    {
    	return newMusic(filename);
    }
    
    public Music newMusic(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(MUSICPATH + filename);
            return new AndroidMusic(assetDescriptor);
        } catch (IOException e) {
        	Log.e("CSound", "Couldn't load music '" + filename + "'");
        	return null;
            //throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }
    
    public Sound newSound(String filename) {
        try {
        	Sound obj = m_mapSounds.get(filename);
        	if(obj==null)
        	{
	            AssetFileDescriptor assetDescriptor = assets.openFd(SOUNDPATH + filename);
	            int soundId = soundPool.load(assetDescriptor, 0);
	            obj = new AndroidSound(soundPool, soundId); 
	            m_mapSounds.put(filename, obj);
        	}
        	else
        	{
        		AndroidSound newobj = new AndroidSound(((AndroidSound)obj).soundPool, ((AndroidSound)obj).soundId);
        		newobj.m_volume = 1.0f;
        		newobj.bLoop = false;
        		obj = (Sound)newobj;
        	}
        	return obj;
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
    }
    
    public void preloadMusic(String filename)
    {
    	if(musicTrack == null)
    	{
    		musicTrack = loadMusicWithName(filename);
    		musicTrack.setLooping(true);
    	}
    }
    
    public void playMusic(String filename)
    {
    	if(musicTrack == null)
    	{
    		musicTrack = loadMusicWithName(filename);
    		musicTrack.setLooping(true);
    	}
    	musicTrack.setVolume(1.0f);
    	musicTrack.play();
    }
    
    public void stopMusic()
    {
    	if(musicTrack!=null)
    	{
    		musicTrack.dispose();
    		musicTrack = null;
    	}
    }
    
    public void unloadSounds()
    {
    	if(m_mapSounds==null)
    		return;
    	
		Set<String> keySet = m_mapSounds.keySet();
		
		Iterator<String> iter = keySet.iterator();
		while(iter.hasNext())
		{
			String key = iter.next();
			Sound snd = m_mapSounds.get(key);
			snd.dispose();
		}
    		 
		this.soundPool.release();
		this.soundPool = new SoundPool(30, AudioManager.STREAM_MUSIC, 0);
		m_mapSounds.clear();
    }
    
    public int mSensorCounter = 0;
    public final int LEVELCOUNTERLOOP = 20;
    public float mLevel;
    public SoundMeter mSensor;
    public float getCurrentLevelMeter()
    {
    	if(mSensorCounter % LEVELCOUNTERLOOP == 0)
    	{
    		mLevel = 10;
    	}
    	
    	mSensorCounter = (mSensorCounter + 1 ) % LEVELCOUNTERLOOP;
    	return mLevel;
    }

}
