package com.tiseno.sweetdream.sound;

import android.media.SoundPool;

public class AndroidSound implements Sound {
    int soundId;
    boolean bLoop;
    SoundPool soundPool;
    float m_volume = 1.0f;
    int streamID;
    
    public AndroidSound(SoundPool soundPool,int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
        streamID = 0;
    }

    @Override
    public void play(float volume) {
    	if(streamID == 0)
    	{
    		if(volume > 0)
    		{
		    	if(bLoop == true)
		    		streamID = soundPool.play(soundId, volume, volume, 0, -1, 1);
		    	else
		    		streamID = soundPool.play(soundId, volume, volume, 0, 0, 1);
    		}
    	}
    }
    
    @Override
    public void playForce(float volume) {
    	if(streamID > 0)
    	{
    		soundPool.stop(streamID);
    		streamID = 0;
    	}
    	play(volume);
    }

    @Override
    public void dispose() {
    	stop();
        soundPool.unload(soundId);
        streamID = 0;
    }
    
    @Override
    public void setLooping(boolean b) 
    {
    	bLoop = b;
    }

	@Override
	public void play() {
		play(m_volume);
	}

	@Override
	public void playForce() {
		// TODO Auto-generated method stub
		playForce(m_volume);
	}

	@Override
	public void setVolume(float volume) {
		//m_volume = volume;
		if(volume < 0.2f)
			m_volume = 0;
		m_volume = 1.0f;
	}

	@Override
	public void stop() {
		if(streamID > 0)
			soundPool.stop(streamID);
		streamID = 0;
	}

}
