package com.tiseno.sweetdream.sound;

public interface Sound {
	
    public void play(float volume);
    public void play();
    
    public void playForce(float volume);
    public void playForce();
    
    public void setVolume(float volume);
    public void setLooping(boolean bool);
    
    public void stop();
    
    public void dispose();
}
