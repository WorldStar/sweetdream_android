package com.tiseno.sweetdream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class SDLeftArrow {

	private Context m_context = null;
	private Bitmap img = null;
	
	private float anchor_x = 0;
	private float anchor_y = 0;
	
	private final int foreward_status = 100;
	private final int backward_status = 101;
	private final int reward_status = 102;
	
	private int anim_status = foreward_status;
	
	private long foreward_time = 0;
	private long backward_time = 0;
	private long reward_time = 0;
	
	private long total_anim_time = 0;
	private boolean isVisible = false;
	
	public SDLeftArrow(Context context)
	{
		m_context = context;
		loadResource();
	}
	
	private void loadResource()
	{
		isVisible = false;
		anchor_x = Utils.m_screenWidth * 0.12f -  (70.0f * Utils.m_screenScaleX) / 2.0f;
		anchor_y = (Utils.m_screenHeight * 8.0f) / 15.0f;
		img = Utils.loadScreenScaledBitmapXY(m_context, "img/btn_more_left.png");		
	}
	
	public void drawCanvas(Canvas canvas, Paint paint)
	{
		if(!isVisible)
			return;
		
		if(img != null)
		{
			if(total_anim_time == 0)
				total_anim_time = System.currentTimeMillis();
			
			if((System.currentTimeMillis() - total_anim_time) < 3000)
			{
				canvas.drawBitmap(img, anchor_x, anchor_y, paint);
				return;
			}
			
			if(anim_status == foreward_status)
			{
				if(foreward_time == 0)
					foreward_time = System.currentTimeMillis();
				
				float percent = (System.currentTimeMillis() - foreward_time) / 200.0f;
				if(percent >= 1)
				{
					foreward_time = 0;
					anim_status = backward_status;
					canvas.save();
					canvas.translate(-20.0f * Utils.m_screenScaleX, 0);
				}else
				{
					canvas.save();
					canvas.translate(-20.0f * Utils.m_screenScaleX * percent, 0);
				}
				
			}else if(anim_status == backward_status)
			{
				if(backward_time == 0)
					backward_time = System.currentTimeMillis();
				
				float percent = (System.currentTimeMillis() - backward_time) / 200.0f;
				if(percent >= 1)
				{
					backward_time = 0;
					anim_status = reward_status;
					canvas.save();
					canvas.translate(10.0f * Utils.m_screenScaleX, 0);
				}else
				{
					canvas.save();
					canvas.translate(30.0f * Utils.m_screenScaleX * percent - 20.0f * Utils.m_screenScaleX, 0);
				}
				
			}else if(anim_status == reward_status)
			{
				if(reward_time == 0)
					reward_time = System.currentTimeMillis();
				
				float percent = (System.currentTimeMillis() - reward_time) / 200.0f;
				if(percent >= 1)
				{
					reward_time = 0;
					anim_status = foreward_status;
					
					canvas.save();
					canvas.translate(0, 0);
					
					total_anim_time = System.currentTimeMillis();
				}else
				{
					canvas.save();
					canvas.translate(-10.0f * Utils.m_screenScaleX * percent + 10.0f * Utils.m_screenScaleX, 0);
				}
			}
			
			canvas.drawBitmap(img, anchor_x, anchor_y, paint);
			
			canvas.restore();
		}
	}
	
	public void destroy()
	{
		if(img != null)
		{
			img.recycle();
			img = null;
		}
	}

	public boolean contains(float x, float y) {
		// TODO Auto-generated method stub
		boolean contained = false;
		
		if(!isVisible)
			return contained;
		
		RectF rectf = new RectF(anchor_x, anchor_y, anchor_x + img.getWidth(), anchor_y + img.getHeight());
		contained = rectf.contains(x, y);
		return contained;
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		isVisible  = b;
	}
}
