package com.tiseno.sweetdream;

import com.tiseno.sweetdream.sound.CSound;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.SurfaceHolder.Callback;

public class SDStartView extends SurfaceView implements Callback{
	
	private Context m_context = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	// for logo image
	private Bitmap m_img_bg_default = null;
	//for start Recovery
	private Bitmap m_img_bar_top = null;
	private SDIntroPageSprite m_sprt_intropage = null;
	private SDButtonSprite m_sprt_start_button = null;
	
	private boolean isStarted = true;
	
	private long anim_time = 0;
	
	//touch listener
	private OnTouchListener m_touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			switch(event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					touchDownAnalysis(event.getX(), event.getY());
					break;
				}
				case MotionEvent.ACTION_MOVE:
				{
					touchMoveAnalysis(event.getX(), event.getY());
					break;
				}
				case MotionEvent.ACTION_UP:
				{
					touchUpAnalysis(event.getX(), event.getY());
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					break;
				}
			}
			
			return true;
		}
	};
	
	private void touchDownAnalysis(float x, float y)
	{

		if(m_sprt_start_button == null || m_sprt_intropage == null)
			return;
		
		if(m_sprt_start_button.contains(x, y))
		{
			m_sprt_start_button.setTouchDown(x, y);
		}
		if(m_sprt_intropage.contains(x, y))
		{
			m_sprt_intropage.setTouchDown(x, y);
		}

	}
	
	private void touchMoveAnalysis(float x, float y)
	{
		if(m_sprt_start_button == null || m_sprt_intropage == null)
			return;
		
		m_sprt_start_button.setTouchMove(x, y);
		m_sprt_intropage.setTouchMove(x, y);
		return;

	}
	
	private void touchUpAnalysis(float x, float y)
	{

		if(m_sprt_start_button == null || m_sprt_intropage == null)
			return;
		
		m_sprt_start_button.setTouchUp(x, y);
		m_sprt_intropage.setTouchUp(x, y);

	}
	
	public SDStartView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		m_context = context;
		
		isStarted = true;
		
		loadStartDiscover();
		
		m_holder = getHolder();
		m_holder.addCallback(this);
		
		setOnTouchListener(m_touchListener);
		
	}
	
	private void loadStartDiscover()
	{
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapXY(m_context, "img/bg_default.png");
		if(m_img_bar_top == null)
			m_img_bar_top = Utils.loadScreenScaledBitmapXY(m_context, "img/bar_top.png");
		if(m_sprt_intropage == null)
			m_sprt_intropage = SDIntroPageSprite.createFromAsset(m_context, "img/intropage.png");
		if(m_sprt_start_button == null)
			m_sprt_start_button = SDButtonSprite.createFromAsset(m_context, "img/btn_start_now.png");			
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.setDaemon(true);
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		m_drawThread.m_shouldExit = true;
		
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResource();
	}
	
	private void removeResource()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
			
		}
		if(m_img_bar_top != null)
		{
			m_img_bar_top.recycle();
			m_img_bar_top = null;
		}
			
		if(m_sprt_intropage != null)
		{
			m_sprt_intropage.destroy();
			m_sprt_intropage = null;
		}
		if(m_sprt_start_button != null)
		{
			m_sprt_start_button.destroy();
			m_sprt_start_button = null;
		}
		
	}
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		
		public void run(){
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
//				try {
//					sleep(30);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}				

			}
		}
		
		public void drawEngine()
		{
			loadStartDiscover();
			drawStartDiscover();
		}
		
		private void drawStartDiscover()
		{
			//draw bg
			m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			if(m_sprt_intropage != null)
			{
				// draw intro page
				if(m_sprt_intropage.getBounds() == null)
				{
					float x = (Utils.m_screenWidth - m_sprt_intropage.getWidth()) / 2.0f;
					float y = m_img_bar_top.getHeight() - 20.0f * Utils.m_screenScaleY;
					RectF rect = new RectF(x, y, x + m_sprt_intropage.getWidth(), y + m_sprt_intropage.getHeight());
					
					m_sprt_intropage.setBounds(rect);		
				}
				
				if(isStarted)
				{
					if(anim_time == 0)
						anim_time = System.currentTimeMillis();
					
					float percent = (System.currentTimeMillis() - anim_time) / 500.0f;
					float dist = -(m_sprt_intropage.getBounds().bottom - m_sprt_intropage.getBounds().bottom * percent);
					
					if(percent > 1)
					{
						anim_time = 0;
						isStarted = false;
						dist = 0;
					}
					m_canvas.save();
					m_canvas.translate(0,  dist);
					m_sprt_intropage.drawSprite(m_canvas, m_paint);					
					m_canvas.restore();
					
				}else
					m_sprt_intropage.drawSprite(m_canvas, m_paint);
			}
			
			if(m_img_bar_top != null)
			{
				// draw top bar
				m_canvas.drawBitmap(m_img_bar_top, 0, 0, m_paint);
			}
			
			// draw start button
			if(m_sprt_start_button != null)
			{
				if(m_sprt_start_button.getBounds() == null)
				{
					float x = (Utils.m_screenWidth - m_sprt_start_button.getWidth()) / 2.0f;
					float y = (Utils.m_screenHeight + m_sprt_intropage.getHeight() - m_sprt_start_button.getHeight()) / 2.0f + 20.0f * Utils.m_screenScaleY;
					RectF rect = new RectF(x, y, x + m_sprt_start_button.getWidth(), y + m_sprt_start_button.getHeight());
					m_sprt_start_button.setBounds(rect);
					m_sprt_start_button.setNavigateHandler(buttonNextNavigateHandler);
				}

				m_sprt_start_button.drawSprite(m_canvas, m_paint);
			}

		}
		
    	private Handler buttonNextNavigateHandler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub
				m_shouldExit = true;
				
				// TODO Auto-generated method stub				
				((SDStartActivity)m_context).overridePendingTransition(0, R.anim.exitanim);
				
				ShareData.Activity_Navigator = true;
				
				((SDStartActivity)m_context).finish();
				Intent intent = new Intent(m_context, SDMainActivity.class);
				m_context.startActivity(intent);				
				return true;
			}
		});
		

	}

}
