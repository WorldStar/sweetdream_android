package com.tiseno.sweetdream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class SDSplashView extends View {

	Context m_context = null;
	private Bitmap m_img_bg_default = null;
	private Bitmap m_img_splash_img_1 =null;
	private Bitmap m_img_splash_img_2 = null;
	private Paint m_paint = null;
	
	public SDSplashView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		m_paint = new Paint();
		m_paint.setDither(true);
		m_paint.setAntiAlias(true);
		
		loadResource();
	}

	private void loadResource() {
		// TODO Auto-generated method stub
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapXY(m_context, "img/bg_default.png");
		if(m_img_splash_img_1 == null)
			m_img_splash_img_1 = Utils.loadScreenScaledBitmapY(m_context, "img/splash_img_1.png");
		if(m_img_splash_img_2 == null)
			m_img_splash_img_2 = Utils.loadScreenScaledBitmapY(m_context, "img/splash_img_2.png");
	}
	
	private void removeResource() {
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
		}
		if(m_img_splash_img_1 == null)
		{
			m_img_splash_img_1.recycle();
			m_img_splash_img_1 = null;
			
		}
		if(m_img_splash_img_2 == null)
		{
			m_img_splash_img_2.recycle();
			m_img_splash_img_2 = null;
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub				
		super.onDraw(canvas);
		
		//draw bg
		if(m_img_bg_default != null)
			canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
		
		float x = 0;
		//draw splash image1
		if(m_img_splash_img_1 != null)
		{
			x = (Utils.m_screenWidth - m_img_splash_img_1.getWidth()) / 2.0f;
			canvas.drawBitmap(m_img_splash_img_1, x, 127 * Utils.m_screenScaleY, m_paint);
		}
		
		//draw splash image2
		if(m_img_splash_img_2 != null)
		{
			x = Utils.m_screenWidth - m_img_splash_img_2.getWidth() - 10.0f * Utils.m_screenScaleY;
			float y = Utils.m_screenHeight - m_img_splash_img_2.getHeight() - 20.0f * Utils.m_screenScaleY;
			canvas.drawBitmap(m_img_splash_img_2, x, y, m_paint);
		}
		
	}


}
