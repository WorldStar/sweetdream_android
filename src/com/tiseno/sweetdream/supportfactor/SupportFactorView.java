package com.tiseno.sweetdream.supportfactor;

import com.tiseno.sweetdream.SDButtonSprite;
import com.tiseno.sweetdream.SDMainActivity;
import com.tiseno.sweetdream.SDStartActivity;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.movie.scene_first;
import com.tiseno.sweetdream.sound.CSound;

import flip.testFlip;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class SupportFactorView extends SurfaceView implements SurfaceHolder.Callback{
	private Context mContext = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	private Typeface font1 = null;
	
	private Bitmap m_img_bg_default = null;
	private Bitmap m_img_bar_top = null;
	private Bitmap m_img_result_page = null;
	private Bitmap m_result_tag = null;
	private SDButtonSprite m_btn_play = null;
	private Bitmap m_floormat_slanted = null;
	private SDButtonSprite m_btn_pillow =null;
	private SDButtonSprite m_btn_repeat = null;
	
	private Bitmap m_img_arrow = null;
	
	//show pillow
	private Bitmap m_tab_pillow = null;
	private SDButtonSprite m_pillow_close = null;
	
	private long rotate_cycle = 4000;
	private long rotate_time = 0;
	
	boolean show_pillow = false;
	
	private OnTouchListener m_touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			
			float x = event.getX();
			float y = event.getY();
			
			switch(event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					if(show_pillow)
					{
						if(m_pillow_close.contains(x, y))
						{
							m_pillow_close.setTouchDown(x, y);
						}
						break;
					}
					
					if(m_btn_play.contains(x, y))
					{
						m_btn_play.setTouchDown(x, y);
					}
					
					if(m_btn_pillow.contains(x, y))
					{
						m_btn_pillow.setTouchDown(x, y);
					}
					
					if(m_btn_repeat.contains(x, y))
					{
						m_btn_repeat.setTouchDown(x, y);
					}					

					break;
				}
				case MotionEvent.ACTION_MOVE:
				{
					if(show_pillow)
					{
						m_pillow_close.setTouchMove(x, y);
						break;
					}
					
					m_btn_play.setTouchMove(x, y);					
					m_btn_pillow.setTouchMove(x, y);
					m_btn_repeat.setTouchMove(x, y);
					
					break;
				}
				case MotionEvent.ACTION_UP:
				{
					if(show_pillow)
					{
						if(m_pillow_close.contains(x, y))
						{
							m_pillow_close.setTouchUp(x, y);
							CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
						}
						break;
					}
					if(m_btn_play.contains(x, y))
					{
						m_btn_play.setTouchUp(x, y);
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					}
					
					if(m_btn_pillow.contains(x, y))
					{
						m_btn_pillow.setTouchUp(x, y);
						CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
					}
					
					if(m_btn_repeat.contains(x, y))
					{
						m_btn_repeat.setTouchUp(x, y);
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					}
					
					if(m_floormat_slanted != null)
					{						
						RectF r = new RectF();
						r.left = 0;
						r.top = Utils.m_screenHeight - m_floormat_slanted.getHeight();
						r.right = Utils.m_screenWidth;
						r.bottom = Utils.m_screenHeight;	
						
						if(r.contains(x, y))
						{
							m_drawThread.m_shouldExit = true;
							
							CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
							
							ShareData.Activity_Navigator = true;
							
							((SupportFactor)mContext).finish();
							Intent intent = new Intent(mContext, testFlip.class);
							mContext.startActivity(intent);
							
						}
					}
					break;
				}
			}
			
			return true;
		}
	};
	
	public SupportFactorView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
		
		loadResource();
		
		m_holder = getHolder();		
		m_holder.addCallback(this);
		
		setOnTouchListener(m_touchListener);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		m_drawThread.m_shouldExit = true;
		
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResource();
	}	
		
	private void loadResource()
	{
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapXY(mContext, "img/bg_default.png");
		
		if(m_img_bar_top == null)
			m_img_bar_top = Utils.loadScreenScaledBitmapXY(mContext, "img/bar_top.png");		

		if(m_img_result_page == null)
			m_img_result_page = Utils.loadScreenScaledBitmapY(mContext, "img/resultpage.png");
		
		if(font1 == null)
		{
			font1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/CreteRound-Regular.otf");
		}		
		
		if(m_result_tag == null)
		{
			m_result_tag = Utils.loadScreenScaledBitmapY(mContext, "img/result_page_tag.png");
		}
		
		if(m_btn_play == null)
		{
			m_btn_play = SDButtonSprite.createFromAsset(mContext, "img/btn_play.png");
			m_btn_play.setNavigateHandler(mPlayHandler);
		}
		
		if(m_btn_pillow == null)
		{
			m_btn_pillow = SDButtonSprite.createFromAsset(mContext, "img/btn_pillow.png");
			m_btn_pillow.setNavigateHandler(mPillowHandler);
		}
		
		if(m_btn_repeat == null)
		{
			m_btn_repeat = SDButtonSprite.createFromAsset(mContext, "img/btn_repeat.png");
			m_btn_repeat.setNavigateHandler(mRepeatHandler);
		}
		
		if(m_floormat_slanted == null)
		{
			m_floormat_slanted = Utils.loadScreenScaledBitmapXY(mContext, "img/floormat_slanted.png");
		}
		
		//show pillow resource
		if(m_tab_pillow == null)
		{
			m_tab_pillow = Utils.loadScreenScaledBitmapY(mContext, "img/tab_pillow.png");
		}
		if(m_pillow_close == null)
		{
			m_pillow_close = SDButtonSprite.createFromAsset(mContext, "img/btn_close.png");
			m_pillow_close.setNavigateHandler(mClosePillowHandler);
		}
		
		if(m_img_arrow == null)
		{
			m_img_arrow = Utils.loadScreenScaledBitmapXY(mContext, "img/arrow.png");
		}
	}
	
	public void removeResource()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
		}
		
		if(m_img_bar_top != null)
		{
			m_img_bar_top.recycle();
			m_img_bar_top = null;
		}

		if(m_img_result_page != null)
		{
			m_img_result_page.recycle();
			m_img_result_page = null;
		}
		
		if(font1 != null)
		{
			font1 = null;
		}		
		
		if(m_result_tag != null)
		{
			m_result_tag = Utils.loadScreenScaledBitmapXY(mContext, "img/result_page_tag.png");
		}
		
		if(m_btn_play != null)
		{
			m_btn_play.destroy();
			m_btn_play = null;
		}
		
		if(m_btn_pillow != null)
		{
			m_btn_pillow.destroy();
			m_btn_pillow = null;
		}
		
		if(m_btn_repeat != null)
		{
			m_btn_repeat.destroy();
			m_btn_repeat = null;
		}
		
		if(m_floormat_slanted != null)
		{
			m_floormat_slanted.recycle();
			m_floormat_slanted = null;
		}
		
		//show pillow resource
		if(m_tab_pillow != null)
		{
			m_tab_pillow.recycle();
			m_tab_pillow = null;
		}
		if(m_pillow_close != null)
		{
			m_pillow_close.destroy();
			m_pillow_close = null;
		}	
		
		if(m_img_arrow != null)
		{
			m_img_arrow.recycle();
			m_img_arrow = null;
		}
	}
	
	Handler mPlayHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
			((SupportFactor)mContext).finish();
			Intent intent = new Intent(mContext, scene_first.class);
			((SupportFactor)mContext).startActivity(intent);
			
			return false;
		}
	} );
	
	Handler mPillowHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			show_pillow = true;
			return false;
		}
	});
	
	Handler mClosePillowHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			show_pillow = false;
			return false;
		}
	});
	
	Handler mRepeatHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
			((SupportFactor)mContext).finish();
			
			ShareData.Selected_Gender = ShareData.Gender_Mail;
			ShareData.Selected_Weight = -1;
			ShareData.Selected_Height = -1;
			ShareData.SLEEPING_POSITION = -1;
			
			Intent intent = new Intent(mContext, SDStartActivity.class);
			((SupportFactor)mContext).startActivity(intent);
			
			return false;
		}
	});
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		private Paint m_factorPaint = new Paint();
		
		public void run()
		{
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			m_factorPaint.setAntiAlias(true);
			m_factorPaint.setDither(true);			
			
			while(!m_shouldExit)
			{
				if(m_shouldExit)
					break;
				
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						loadResource();
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
				try {
					sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		}

		private void drawEngine() {
			// TODO Auto-generated method stubl
			//draw bg
			if(m_img_bg_default != null)
				m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			if(m_img_result_page != null)
				m_canvas.drawBitmap(m_img_result_page, (Utils.m_screenWidth - m_img_result_page.getWidth())/2.0f, 20.0f * Utils.m_screenScaleY, m_paint);
			
			// draw top bar
			if(m_img_bar_top != null)
				m_canvas.drawBitmap(m_img_bar_top, 0, 0, m_paint);
			
			if(m_btn_pillow != null)
			{
				RectF r = new RectF();
				r.left = 6 * Utils.m_screenScaleX;
				r.top = 6 * Utils.m_screenScaleY;
				r.right = r.left + m_btn_pillow.getWidth();
				r.bottom = r.top + m_btn_pillow.getHeight();
				m_btn_pillow.setBounds(r);
				m_btn_pillow.drawSprite(m_canvas, m_paint);
			}
			
			if(m_btn_repeat != null)
			{
				RectF r = new RectF();
				r.left = Utils.m_screenWidth - (6 * Utils.m_screenScaleY + m_btn_repeat.getWidth());
				r.top = 6 * Utils.m_screenScaleY;
				r.right = r.left + m_btn_repeat.getWidth();
				r.bottom = r.top + m_btn_repeat.getHeight();
				m_btn_repeat.setBounds(r);
				m_btn_repeat.drawSprite(m_canvas, m_paint);
			}
			
			m_paint.setTypeface(font1);
			m_paint.setColor(0xff555555);
			m_paint.setTextAlign(Align.CENTER);
			m_paint.setTextSize(28 * Utils.m_screenScaleY);
			
			float x = Utils.m_screenWidth / 2.0f;
						
			String top_str = "You are " + ShareData.FACTOR_STRING;
			m_canvas.drawText(top_str, x, 130.0f * Utils.m_screenScaleY, m_paint);
			
			//draw sleep pos string
			m_canvas.drawText(ShareData.SLEEPING_POSITION_STRING, x, 170.0f * Utils.m_screenScaleY, m_paint);
			
			m_paint.setTextSize(40 * Utils.m_screenScaleY);
			m_paint.setColor(0xff333333);
			m_canvas.drawText("Look for the mattress with", x, 220 * Utils.m_screenScaleY, m_paint);
			
			//draw result tag
			float x_tag = (Utils.m_screenWidth - m_result_tag.getWidth()) / 2.0f;
			float y_tag = 250 * Utils.m_screenScaleY;
			m_canvas.drawBitmap(m_result_tag, x_tag, y_tag, null);
			
			//draw support factor
			float x_factor = x_tag + 224 * Utils.m_screenScaleY;
			float y_factor = y_tag + m_result_tag.getHeight() / 2.0f + 5 * Utils.m_screenScaleY;
			String factor = String.valueOf(ShareData.SUPPORTED_FACTOR_VALUE);
			m_factorPaint.setTextSize(180 * Utils.m_screenScaleY);
			m_factorPaint.setColor(Color.WHITE);
			m_factorPaint.setTextAlign(Align.CENTER);
			m_canvas.drawText(factor, x_factor, y_factor, m_factorPaint);	
			
			//draw bottom string
			float y_bottom = y_tag + m_result_tag.getHeight() + 40 * Utils.m_screenScaleY;
			String str_bottom = "To maximize your recovery while you sleep.";
			m_paint.setColor(0xff555555);
			m_paint.setTextSize(28 * Utils.m_screenScaleY);
			
			m_canvas.drawText(str_bottom, x, y_bottom, m_paint);
			
			if(rotate_time == 0)
				rotate_time = System.currentTimeMillis();
			
			if(System.currentTimeMillis() - rotate_time > rotate_cycle)
				rotate_time = System.currentTimeMillis();
			
			float percent = (System.currentTimeMillis() - rotate_time) / 800.0f;
			float angle = 360.0f * percent;
			if(percent > 1)
			{
				angle = 0;
			}
			
			//draw play button
			m_btn_play.setBounds(new RectF(x_tag + 430 * Utils.m_screenScaleY, y_tag + m_result_tag.getHeight() / 2.0f, 
					x_tag + 430 * Utils.m_screenScaleY + m_btn_play.getWidth(), y_tag + m_result_tag.getHeight() / 2.0f + m_btn_play.getHeight()));
			
			m_canvas.save();
			m_canvas.rotate(angle, m_btn_play.getBounds().centerX(), m_btn_play.getBounds().centerY());
			m_btn_play.drawSprite(m_canvas, null);
			
			m_canvas.restore();
			
			//draw floormat
			m_canvas.drawBitmap(m_floormat_slanted, (Utils.m_screenWidth - m_floormat_slanted.getWidth()) / 2.0f, Utils.m_screenHeight - m_floormat_slanted.getHeight(), null);
			
			m_paint.setColor(Color.WHITE);
			m_paint.setTextSize(36.0f * Utils.m_screenScaleY);
			
			m_canvas.drawText("Frequently Asked Questions", x, Utils.m_screenHeight - 96.0f * Utils.m_screenScaleY, m_paint);
			
			float arrow_x = (Utils.m_screenWidth - m_img_arrow.getWidth()) / 2.0f;
			float arrow_y = Utils.m_screenHeight - 66.0f * Utils.m_screenScaleY;
			m_canvas.drawBitmap(m_img_arrow, arrow_x, arrow_y, m_paint);
			
			if(show_pillow)
			{
				m_canvas.drawARGB(99, 0, 0, 0);
				
				// draw dialog box
				float p_x = (Utils.m_screenWidth - m_tab_pillow.getWidth()) / 2.0f;
				float p_y = (Utils.m_screenHeight - m_tab_pillow.getHeight()) / 2.0f;
				m_canvas.drawBitmap(m_tab_pillow, p_x, p_y, null);
				
				// draw close button
				float b_x = p_x - m_pillow_close.getWidth() / 2.0f;
				float b_y = p_y - m_pillow_close.getHeight() / 2.0f;
				RectF rect = new RectF();
				rect.left = b_x;
				rect.top = b_y;
				rect.right = b_x + m_pillow_close.getWidth();
				rect.bottom = b_y + m_pillow_close.getHeight();
				m_pillow_close.setBounds(rect);
				m_pillow_close.drawSprite(m_canvas, null);
				
				// draw recommended pillow string
				m_paint.setTextSize(56 * Utils.m_screenScaleY);
				m_paint.setColor(Color.BLACK);
				String str_pillow = ShareData.RECOMMENDED_PILLOW;
				m_canvas.drawText(str_pillow, Utils.m_screenWidth / 2.0f, Utils.m_screenHeight / 2.0f + 40 * Utils.m_screenScaleY, m_paint);
			}
		}
		
	}

}
