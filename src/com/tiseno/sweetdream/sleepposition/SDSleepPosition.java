package com.tiseno.sweetdream.sleepposition;

import com.tiseno.sweetdream.R;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.sound.CSound;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class SDSleepPosition extends Activity {

	private SDSleepPostionView sleepView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		boolean backward = intent.getBooleanExtra("backward", false);
		if(backward)
			overridePendingTransition(R.anim.enteranim_right, R.anim.exitanim_right);
		else
			overridePendingTransition(R.anim.enteranim, R.anim.exitanim);
		
		sleepView = new SDSleepPostionView(this);
		setContentView(sleepView);
	}	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.pause();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.play();
		}else
		{
			ShareData.Activity_Navigator = false;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return sleepView.TouchEvent(event);
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
		{
			new AlertDialog.Builder(this).setTitle("SweetDream")
			.setMessage("Do you want to close this app?")
			.setPositiveButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			})
			.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					CSound.sharedSoundController().stopMusic();
					SDSleepPosition.this.finish();
				}
			} ).show();
			
			return true;
		}
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_HOME)
		{
			CSound.sharedSoundController().stopMusic();
			return true;
		}
		
		return false;
	}
}
