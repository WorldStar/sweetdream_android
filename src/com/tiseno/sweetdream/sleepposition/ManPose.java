package com.tiseno.sweetdream.sleepposition;

import com.tiseno.sweetdream.SDLeftArrow;
import com.tiseno.sweetdream.SDRightArrow;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;

public class ManPose {
	Context mContext = null;
	
	public final int sleeperStatus1 = 0;
	public final int sleeperStatus2 = 1;
	public final int sleeperStatus3 = 2;
	
	public final int anim_none = -1;
	public final int anim_left = 1;
	public final int anim_right = 2;
	
	public int current_status = 0;
	
	private Bitmap m_backsleeper = null;
	private Bitmap m_sidesleeper = null;
	private Bitmap m_stomachsleeper = null;
	private Bitmap m_pillow = null;
	
	private SDRightArrow m_right_arrow;
	private SDLeftArrow m_left_arrow;
	
	private float anchor_x;
	private float anchor_y;

	private boolean isStartingMoveAnimation = false;
	private long move_time = 0;
	private int anim_status = anim_none;
	
	private float total_diff_x = 0;

	private boolean isStartingSelectAnimation = false;

	private long zoom_time = 0;
	private float zoom_val = 1;

	private Handler mNextActivityHandler = null;
	
	public ManPose(Context context)
	{
		mContext = context;
		isStartingMoveAnimation = false;
		isStartingSelectAnimation = false;
		move_time = 0;
		anim_status = anim_none;
		total_diff_x = 0;
		
		zoom_time = 0;
		zoom_val = 1;
		
		current_status = ShareData.SLEEPING_POSITION;
		if(current_status == 0 || current_status == -1)
		{
			total_diff_x = 0;
			current_status = 0;
			ShareData.SLEEPING_POSITION = 0;
		}
		else if(current_status == 1)
			total_diff_x = -Utils.m_screenWidth;
		else if(current_status == 2)
			total_diff_x = -Utils.m_screenWidth * 2;
			
		
		loadResource();
	}
	
	private void loadResource()
	{
		if(m_backsleeper == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
				m_backsleeper = Utils.loadScreenScaledBitmapY(mContext, "img/male_backsleeper.png");
			else
				m_backsleeper = Utils.loadScreenScaledBitmapY(mContext, "img/female_backsleeper.png");
		}
		if(m_sidesleeper == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
				m_sidesleeper = Utils.loadScreenScaledBitmapY(mContext, "img/male_sidesleeper.png");
			else
				m_sidesleeper = Utils.loadScreenScaledBitmapY(mContext, "img/female_sidesleeper.png");
		}
		if(m_stomachsleeper == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
				m_stomachsleeper = Utils.loadScreenScaledBitmapY(mContext, "img/male_stomachsleeper.png");
			else
				m_stomachsleeper = Utils.loadScreenScaledBitmapY(mContext, "img/female_stomachsleeper.png");
		}
		
		if(m_right_arrow == null)
		{
			m_right_arrow = new SDRightArrow(mContext);				
		}
		
		if(m_left_arrow == null)
		{
			m_left_arrow = new SDLeftArrow(mContext);
		}
		
		if(m_pillow == null)
		{
			m_pillow = Utils.loadScreenScaledBitmapY(mContext, "img/pillow.png");			
		}
		
	}
	
	public void drawSleepPose(Canvas canvas, Paint paint)
	{
		float scr_w = Utils.m_screenWidth;
		float scr_h = Utils.m_screenHeight;
		
		if(isStartingSelectAnimation)
		{
			if(zoom_time  == 0)
				zoom_time = System.currentTimeMillis();
			zoom_val  = 1.025f;
			if((System.currentTimeMillis() - zoom_time) > 150)
			{
				isStartingSelectAnimation = false;
				zoom_time = 0;
				zoom_val = 1;
				
				if(mNextActivityHandler != null)
					mNextActivityHandler.sendEmptyMessageDelayed(0, 150);
			}
		}
		
		canvas.save();
		canvas.scale(zoom_val, zoom_val, Utils.m_screenWidth/2.0f, Utils.m_screenHeight/2.0f);
		
		if(isStartingMoveAnimation)
		{
			if(move_time == 0)
				move_time = System.currentTimeMillis();
			
			if(anim_status == anim_left)
			{
				float perc = (System.currentTimeMillis() - move_time) / 300.0f;
				if(current_status == sleeperStatus1)
				{
					if(perc > 1.0f)
					{
						total_diff_x = - scr_w;
						current_status = sleeperStatus2;
						anim_status = anim_none;
						isStartingMoveAnimation = false;
						move_time = 0;
						
					}else
						total_diff_x = - scr_w * perc;
				}else if(current_status == sleeperStatus2)
				{
					if(perc > 1.0f)
					{
						total_diff_x = - scr_w * 2;
						current_status = sleeperStatus3;
						anim_status = anim_none;
						isStartingMoveAnimation = false;
						move_time = 0;
					}else
						total_diff_x = - scr_w * perc - scr_w;
				}
			}else if(anim_status == anim_right)
			{
				float perc = (System.currentTimeMillis() - move_time) / 300.0f;
				if(current_status == sleeperStatus2)
				{
					if(perc > 1.0f)
					{
						total_diff_x = 0;
						current_status = sleeperStatus1;
						anim_status = anim_none;
						isStartingMoveAnimation = false;
						move_time = 0;
					}else
						total_diff_x = - scr_w + scr_w * perc;
				}else if(current_status == sleeperStatus3)
				{
					if(perc > 1.0f)
					{
						total_diff_x = - scr_w;
						current_status = sleeperStatus2;
						anim_status = anim_none;
						isStartingMoveAnimation = false;
						move_time = 0;
					}else
						total_diff_x = -scr_w * 2 + scr_w * perc;
				}
			}
		}
		
		
		if(m_pillow != null && m_backsleeper != null)
		{
			float p_x = (scr_w - m_pillow.getWidth()) / 2.0f;
			float p_y = (scr_h - m_backsleeper.getHeight()) / 2.0f;
			canvas.drawBitmap(m_pillow, p_x, p_y, paint);
		}
		
		if(m_backsleeper != null)
		{
			anchor_x = (scr_w - m_backsleeper.getWidth()) / 2.0f + total_diff_x;
			anchor_y = (scr_h - m_backsleeper.getHeight()) / 2.0f;
			canvas.drawBitmap(m_backsleeper, anchor_x, anchor_y, paint);			
		}
		
		if(m_sidesleeper != null)
		{
			float x = anchor_x + scr_w;
			canvas.drawBitmap(m_sidesleeper, x, anchor_y, paint);
		}
		
		if(m_stomachsleeper != null)
		{
			float x = anchor_x + scr_w * 2;
			canvas.drawBitmap(m_stomachsleeper, x, anchor_y, paint);
		}
		
		// draw arrow
		if(current_status == sleeperStatus1)
		{
			m_right_arrow.setVisible(true);
			m_left_arrow.setVisible(false);
			
			if(m_right_arrow != null)
			{
				m_right_arrow.drawCanvas(canvas, paint);
			}
		}else if(current_status == sleeperStatus2)
		{
			m_right_arrow.setVisible(true);
			m_left_arrow.setVisible(true);
			if(m_right_arrow != null)
			{
				m_right_arrow.drawCanvas(canvas, paint);
			}
			if(m_left_arrow != null)
			{
				m_left_arrow.drawCanvas(canvas, paint);
			}
		}else if(current_status == sleeperStatus3)
		{
			m_right_arrow.setVisible(false);
			m_left_arrow.setVisible(true);
			if(m_left_arrow != null)
			{
				m_left_arrow.drawCanvas(canvas, paint);
			}
		}
		
		canvas.restore();
	}
	
	public void setNextActivityHandler(Handler handler)
	{
		mNextActivityHandler = handler;
	}

	public void setTouchedDown(float x, float y) {
		// TODO Auto-generated method stub
		if(isStartingMoveAnimation || isStartingSelectAnimation)
			return;
		
		if(current_status == sleeperStatus1)
		{
			if(m_right_arrow.contains(x, y))
			{
				anim_status = anim_left;
				isStartingMoveAnimation = true;
				ShareData.SLEEPING_POSITION = 1;
				return;
			}
			
		}else if(current_status == sleeperStatus2)
		{
			if(m_right_arrow.contains(x, y))
			{
				anim_status = anim_left;
				isStartingMoveAnimation = true;
				ShareData.SLEEPING_POSITION = 2;
				return;
			}
			if(m_left_arrow.contains(x, y))
			{
				anim_status = anim_right;
				isStartingMoveAnimation = true;
				ShareData.SLEEPING_POSITION = 0;
				return;
			}
		}else if(current_status == sleeperStatus3)
		{
			if(m_left_arrow.contains(x, y))
			{
				anim_status = anim_right;
				isStartingMoveAnimation = true;
				ShareData.SLEEPING_POSITION = 1;
				return;
			}
		}
		
		if(m_backsleeper != null)
		{
			float body_x = (Utils.m_screenWidth - m_backsleeper.getWidth()) / 2.0f;
			float body_y = (Utils.m_screenHeight - m_backsleeper.getHeight()) / 2.0f;
			
			RectF r = new RectF(body_x, body_y, body_x + m_backsleeper.getWidth(), body_y + m_backsleeper.getHeight());
			if(r.contains(x, y))
			{
				zoom_time = 0;
				zoom_val = 1.0f;
				isStartingSelectAnimation  = true;
				return;
			}
		}
	}
	
	public void moveRight()
	{
		if(isStartingMoveAnimation || isStartingSelectAnimation)
			return;
		
		if(current_status == sleeperStatus1)
		{
				anim_status = anim_left;
				isStartingMoveAnimation = true;
				ShareData.SLEEPING_POSITION = 1;
				return;			
		}else if(current_status == sleeperStatus2)
		{
				anim_status = anim_left;
				isStartingMoveAnimation = true;
				ShareData.SLEEPING_POSITION = 2;
				return;
		}
	}
	
	public void moveLeft()
	{
		if(isStartingMoveAnimation || isStartingSelectAnimation)
			return;
		
		if(current_status == sleeperStatus2)
		{
			anim_status = anim_right;
			isStartingMoveAnimation = true;
			ShareData.SLEEPING_POSITION = 0;
			return;
		}else if(current_status == sleeperStatus3)
		{
			anim_status = anim_right;
			isStartingMoveAnimation = true;
			ShareData.SLEEPING_POSITION = 1;
			return;
		}
	}

	public void destroy() {
		// TODO Auto-generated method stub
		if(m_backsleeper != null)
		{
			m_backsleeper.recycle();
			m_backsleeper = null;
		}
		if(m_sidesleeper != null)
		{
			m_sidesleeper.recycle();
			m_sidesleeper = null;
		}
		if(m_stomachsleeper != null)
		{
			m_stomachsleeper.recycle();
			m_stomachsleeper = null;				
		}
		
		if(m_right_arrow != null)
		{
			m_right_arrow.destroy();
			m_right_arrow = null;
		}
		
		if(m_left_arrow != null)
		{
			m_left_arrow.destroy();
			m_left_arrow = null;
		}
		
		if(m_pillow != null)
		{
			m_pillow.recycle();
			m_pillow = null;
		}
	}
}
