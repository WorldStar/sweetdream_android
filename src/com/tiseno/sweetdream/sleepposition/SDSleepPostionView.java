package com.tiseno.sweetdream.sleepposition;

import com.tiseno.sweetdream.R;
import com.tiseno.sweetdream.SDBottomText;
import com.tiseno.sweetdream.SDButtonSprite;
import com.tiseno.sweetdream.SDMainActivity;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.measure.MeasureWeightActivity;
import com.tiseno.sweetdream.sound.CSound;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.GestureDetector.OnGestureListener;

public class SDSleepPostionView extends SurfaceView implements SurfaceHolder.Callback{

	private Context mContext = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	private Bitmap m_bar_top = null;
	private SDBottomText m_sleep_bottom_text1= null;
	private SDBottomText m_sleep_bottom_text2= null;
	
	private Bitmap m_sleep_position_bg = null;
	
	private ManPose m_man_pos = null;
	
	private SDButtonSprite m_prev_scene = null;
	private SDButtonSprite m_next_scene = null;
	
	private GestureDetector m_gestureDetector = null;
	
	private boolean enableTouch = false;
	
	public SDSleepPostionView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}
	
	public SDSleepPostionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public SDSleepPostionView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}


	private void init(Context context) {
		// TODO Auto-generated method stub
		mContext = context;
		
		loadResources();	
		
		m_holder = getHolder();		
		m_holder.addCallback(this);
		
		enableTouch = false;
		
		m_gestureDetector = new GestureDetector(mContext, mGestureListner);
		m_gestureDetector.setIsLongpressEnabled(true);		
		
		(new Handler()).postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				enableTouch = true;
			}
		}, 1500);

	}
	
	private OnGestureListener mGestureListner = new OnGestureListener() {
		
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			if(!enableTouch)
				return true;
						
			if(m_man_pos != null)
			{				
				m_man_pos.setTouchedDown(e.getX(), e.getY());
				CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
			}
			
			if(m_prev_scene != null)
			{
				if(m_prev_scene.contains(e.getX(), e.getY()))
				{
					m_prev_scene.setTouchDown(e.getX(), e.getY());
					m_prev_scene.setTouchUp(e.getX(), e.getY());
					
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
				}
			}
			
			if(m_next_scene != null)
			{
				if(m_next_scene.contains(e.getX(), e.getY()))
				{
					m_next_scene.setTouchDown(e.getX(), e.getY());
					m_next_scene.setTouchUp(e.getX(), e.getY());
					
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
				}
			}
			
			return false;
		}
		
		@Override
		public void onShowPress(MotionEvent e) {
			// TODO Auto-generated method stub		

		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
				float distanceY) {
			// TODO Auto-generated method stub
			
			return false;
		}
		
		@Override
		public void onLongPress(MotionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			// TODO Auto-generated method stub

			if((e2.getX() - e1.getX()) > 0)
			{
				if(m_man_pos != null)
				{
					m_man_pos.moveLeft();
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
				}					
			}else
			{
				if(m_man_pos != null)
				{
					m_man_pos.moveRight();
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
				}	
			}
			return false;
		}
		

		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
						
			return false;
		}	
	};
	
	private Handler mPrevActHandler = new Handler(new Handler.Callback() {		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
            ((SDSleepPosition)mContext).finish();			
			
			Intent intent = new Intent(mContext, SDMainActivity.class);
			
			intent.putExtra("backward", true);
			
			mContext.startActivity(intent);
			
			return true;
		}
	});
	
	public boolean TouchEvent(MotionEvent event)
	{
		boolean returnVal = m_gestureDetector.onTouchEvent(event);		
		return returnVal;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.setDaemon(true);
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		m_drawThread.m_shouldExit = true;
				
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResources();
	}	
	
	private void loadResources()
	{
		if(m_sleep_position_bg == null)
		{
			m_sleep_position_bg = Utils.loadScreenScaledBitmapXY(mContext, "img/bg_floor.png");
		}
		
		if(m_bar_top == null)
		{
			m_bar_top = Utils.loadScreenScaledBitmapXY(mContext, "img/bar_top.png");
		}
		
		if(m_sleep_bottom_text1 == null)
		{
			m_sleep_bottom_text1 = new SDBottomText(mContext, "Tap To Choose your");
		}
		
		if(m_sleep_bottom_text2 == null)
		{
			m_sleep_bottom_text2 = new SDBottomText(mContext, "sleeping position ");
		}
		
		if(m_man_pos == null)
		{
			m_man_pos = new ManPose(mContext);
			m_man_pos.setNextActivityHandler(mNexyActivityHandler);			
		}
		
		if(m_prev_scene == null)
		{
			m_prev_scene = SDButtonSprite.createFromAsset(mContext, "img/btn_prev.png");
			RectF rect = new RectF();
			rect.left = 6 * Utils.m_screenScaleX;
			rect.top = 6* Utils.m_screenScaleY;
			rect.right = rect.left + m_prev_scene.getWidth();
			rect.bottom = rect.top + m_prev_scene.getHeight();
			m_prev_scene.setBounds(rect);
			
			m_prev_scene.setEnable(true);
			m_prev_scene.setNavigateHandler(mPrevActHandler);
		}
		
		if(m_next_scene == null)
		{
			m_next_scene = SDButtonSprite.createFromAsset(mContext, "img/btn_next.png");
			RectF rect = new RectF();
			rect.left = Utils.m_screenWidth - 6 * Utils.m_screenScaleX - m_next_scene.getWidth();
			rect.top = 6* Utils.m_screenScaleY;
			rect.right = rect.left + m_next_scene.getWidth();
			rect.bottom = rect.top + m_next_scene.getHeight();
			m_next_scene.setBounds(rect);
			
			if(ShareData.Selected_Weight > -1)
			{
				m_next_scene.setEnable(true);
				m_next_scene.setNavigateHandler(mNexyActivityHandler);
			}else
				m_next_scene.setEnable(false);
		}
	}
	
	private void removeResources() {
		// TODO Auto-generated method stub
		if(m_sleep_position_bg != null)
		{
			m_sleep_position_bg.recycle();
			m_sleep_position_bg = null;
		}
		
		if(m_bar_top != null)
		{
			m_bar_top.recycle();
			m_bar_top = null;
		}
		
		if(m_sleep_bottom_text1 != null)
		{
			m_sleep_bottom_text1 = null;
		}
		
		if(m_sleep_bottom_text2 != null)
		{
			m_sleep_bottom_text2 = null;
		}
		
		if(m_man_pos != null)
		{
			m_man_pos.destroy();
			m_man_pos = null;
		}		
		
		if(m_prev_scene != null)
		{
			m_prev_scene.destroy();
			m_prev_scene = null;
		}
		
		if(m_next_scene != null)
		{
			m_next_scene.destroy();
			m_next_scene = null;
		}
	}
	
	private Handler mNexyActivityHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {			
			
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;
			
			// TODO Auto-generated method stub
			((SDSleepPosition)mContext).overridePendingTransition(0, R.anim.exitanim);
			
			ShareData.Activity_Navigator = true;
			
			((SDSleepPosition)mContext).finish();
			
			Intent intent = new Intent(mContext, MeasureWeightActivity.class);
			mContext.startActivity(intent);
			
			return true;
		}
	});

	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();

		
		public void run()
		{
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						loadResources();
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
				try {
					sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		private void drawEngine() {
			// TODO Auto-generated method stubl
		
			//draw all
			if(m_sleep_position_bg != null)
			{
				m_canvas.drawBitmap(m_sleep_position_bg, 0,0, m_paint);
			}
			if(m_bar_top != null)
			{
				m_canvas.drawBitmap(m_bar_top, 0, 0, m_paint);
			}
			if(m_sleep_bottom_text1 != null)
			{
				m_sleep_bottom_text1.drawText(m_canvas, 2);
			}
			if(m_sleep_bottom_text2 != null)
			{
				m_sleep_bottom_text2.drawText(m_canvas, 1);
			}
			
			if(m_man_pos != null)
			{
				m_man_pos.drawSleepPose(m_canvas, m_paint);
			}			
			
			//draw navigator button
			if(m_prev_scene != null)
				m_prev_scene.drawSprite(m_canvas, m_paint);
			
			if(m_next_scene != null)
				m_next_scene.drawSprite(m_canvas, m_paint);
		}

	}

}
