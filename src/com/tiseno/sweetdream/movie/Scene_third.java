package com.tiseno.sweetdream.movie;

import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.sound.CSound;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

public class Scene_third extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		Scene_third_view view = new Scene_third_view(this);
		setContentView(view);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.pause();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.play();
		}else
		{
			ShareData.Activity_Navigator = false;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
		{
			new AlertDialog.Builder(this).setTitle("SweetDream")
			.setMessage("Do you want to close this app?")
			.setPositiveButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			})
			.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					CSound.sharedSoundController().stopMusic();
					Scene_third.this.finish();
				}
			} ).show();
			
			return true;
		}
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_HOME)
		{
			CSound.sharedSoundController().stopMusic();
			return true;
		}
		
		return false;
	}
}
