package com.tiseno.sweetdream.movie;

import com.tiseno.sweetdream.SDButtonSprite;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.sound.CSound;
import com.tiseno.sweetdream.supportfactor.SupportFactor;

import android.R.color;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.graphics.Shader.TileMode;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class Scene_fourth_view extends SurfaceView implements SurfaceHolder.Callback {

	private Context m_context = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	private Bitmap m_img_bg_default = null;
	private Bitmap m_img_support_factory = null;
	
	private SDButtonSprite m_btn_next_round = null;
	
	private Typeface font1 = null;
	
	private Bitmap title_weight = null;
	private Bitmap title_support = null;
	
	private int under_val[] = {7,9,11,11,10,8,7,9,12,14,16,18,19,18,16,13,11,10,9,7,6};
	private int normal_val[] = {9,12,15,15,13,11,10,13,16,19,22,24,26,25,22,18,15,14,12,10,9};
	private int over_val[] = {11,15,18,18,16,13,12,16,20,23,27,30,32,31,27,22,19,17,15,12,11};
	private int obese_val[] = {13,18,22,22,19,16,15,19,24,28,33,36,39,37,33,27,22,21,18,15,13};
	
	private boolean start_show_arrow = false;
	
	public Scene_fourth_view(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		start_show_arrow =false;
		
		loadResource();
		
		m_holder = getHolder();
		m_holder.setKeepScreenOn(false);
		m_holder.addCallback(this);
		
		this.setOnTouchListener(m_touchListener);
		
	}
	
	private OnTouchListener m_touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			
			if(m_drawThread.m_shouldExit)
				return true;
			
			float x = event.getX();
			float y = event.getY();
			
			switch(event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					if(m_btn_next_round.contains(x, y))
					{
						m_btn_next_round.setTouchDown(x, y);
					}
					break;
				}
				case MotionEvent.ACTION_MOVE:
				{
					m_btn_next_round.setTouchMove(x, y);
					break;
				}
				case MotionEvent.ACTION_UP:
				{
					if(m_btn_next_round.contains(x, y))
					{
						m_btn_next_round.setTouchUp(x, y);
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					}
					break;
				}
			}
			
			return true;
		}
	};

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		m_drawThread.m_shouldExit = true;
		
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResources();
	}
	
	private void loadResource()
	{
		if(m_img_bg_default == null)
		{			
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
			{
				m_img_bg_default = Utils.loadScreenScaledBitmapYX(m_context, "img/bg_scene_4.png");
			}else
			{
				m_img_bg_default = Utils.loadScreenScaledBitmapYX(m_context, "img/bg_scene_4f.png");
			}
		}
		
		font1 = Typeface.createFromAsset(m_context.getAssets(), "fonts/CreteRound-Regular.otf");
		
		if(m_img_support_factory == null)
		{
			if(ShareData.BMI_STATUS == ShareData.BMI_UNDER)
			{
				m_img_support_factory = Utils.loadScreenScaledBitmapYX(m_context, "img/support_arrow_under.png");				
			}else if(ShareData.BMI_STATUS == ShareData.BMI_NORMAL_1 ||
					ShareData.BMI_STATUS == ShareData.BMI_NORMAL_2 ||
					ShareData.BMI_STATUS == ShareData.BMI_NORMAL_3)
			{
				m_img_support_factory = Utils.loadScreenScaledBitmapYX(m_context, "img/support_arrow_normal.png");
			}else if(ShareData.BMI_STATUS == ShareData.BMI_OVER_1 ||
					ShareData.BMI_STATUS == ShareData.BMI_OVER_2)
			{
				m_img_support_factory = Utils.loadScreenScaledBitmapYX(m_context, "img/support_arrow_over.png");
			}else if(ShareData.BMI_STATUS == ShareData.BMI_OBESE_1 ||
					ShareData.BMI_STATUS == ShareData.BMI_OBESE_2 ||
					ShareData.BMI_STATUS == ShareData.BMI_OBESE_3)
			{
				m_img_support_factory = Utils.loadScreenScaledBitmapYX(m_context, "img/support_arrow_obese.png");
			}
		}
		
		if(m_btn_next_round == null)
		{
			m_btn_next_round = SDButtonSprite.createFromAsset(m_context, "img/btn_next_round.png");
			m_btn_next_round.setNavigateHandler(mNextRoundHandler);
		}
		
		if(title_weight == null)
		{
			title_weight = Utils.loadScreenScaledBitmapXY(m_context, "img/title_weight.png");
		}
		
		if(title_support == null)
		{
			title_support = Utils.loadScreenScaledBitmapXY(m_context, "img/title_support.png");
		}
	}
	
	private void removeResources()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
		}
		
		if(m_img_support_factory != null)
		{
			m_img_support_factory.recycle();
			m_img_support_factory = null;
		}
		
		if(m_btn_next_round != null)
		{
			m_btn_next_round.destroy();
			m_btn_next_round = null;
		}
		
		
		if(title_weight != null)
		{
			title_weight.recycle();
			title_weight = null;
		}
		
		if(title_support != null)
		{
			title_support.recycle();
			title_support = null;
		}
		
	}
	
	private Handler mNextRoundHandler = new Handler(new Handler.Callback() {		
		@Override
		public boolean handleMessage(Message msg) {
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
			// TODO Auto-generated method stub
			((Scene_fourth)m_context).finish();
			Intent intent = new Intent(m_context, SupportFactor.class);
			m_context.startActivity(intent);
			return false;
		}
	});
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		private Paint m_bar_paint = new Paint();		
		private Paint mm_p = new Paint();
		
		private long moving_time = 0;
		private long color_bar_time =0;
		
		public DrawThread() {
			// TODO Auto-generated constructor stub
			mm_p.setTypeface(font1);
			mm_p.setTextAlign(Align.CENTER);			
			mm_p.setAntiAlias(true);		
			
			m_bar_paint.setColor(Color.RED);

		}
		
		public void run(){
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
		
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						m_canvas.drawColor(color.black);
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							Log.e("Scene fourth's surface view", "Unlock failed");
						}
					}
				}
				
//				try {
//					sleep(30);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}
		}
		
		public void drawEngine()
		{			
			loadResource();
			drawResource();
		}		
		
		private void drawResource()
		{
			//draw bg
			m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			//draw press bar
			int bars[] = null;
			if(ShareData.FACTOR_STRING.compareToIgnoreCase("under weight") == 0)
			{
				bars = under_val;
			}else if(ShareData.FACTOR_STRING.compareToIgnoreCase("normal") == 0)
			{
				bars = normal_val;
			}else if(ShareData.FACTOR_STRING.compareToIgnoreCase("over weight") == 0)
			{
				bars = over_val;
			}else if(ShareData.FACTOR_STRING.compareToIgnoreCase("obese") == 0)
			{
				bars = obese_val;
			}
			
			if(bars != null)
			{
				if(color_bar_time == 0)
					color_bar_time = System.currentTimeMillis();
				
				float percent = (float)(System.currentTimeMillis() - color_bar_time) / 1000.0f;
				//			Bitmap head = m_head1;
				if(percent > 1)
				{
					start_show_arrow = true;
					percent = 1;
				}
				
				float pi_x = (Utils.m_screenHeight - m_img_support_factory.getWidth()) / 2.0f;
				float pi_y = 460 * Utils.m_screenScaleX - m_img_support_factory.getHeight();
				float pi_w = m_img_support_factory.getWidth();
				float pi_space = pi_w / 21.0f;
				float pi_bar_space = pi_space / 3.0f;
				
				for(int i = 0; i < 21; i++)
				{
					float bar_x = pi_x + pi_space * i + pi_bar_space;
					float bar_y = pi_y;
					float bar_h = bars[i] * 2 * Utils.m_screenScaleX;
					float bar_w = pi_bar_space;
					m_bar_paint.setShader(new LinearGradient(bar_x, bar_y, bar_x + bar_w, bar_y + bar_h, Color.RED, Color.YELLOW, TileMode.MIRROR));
					
					m_canvas.save();
					m_canvas.scale(1, percent, bar_x + bar_w / 2.0f, bar_y);
					m_canvas.drawRect(bar_x, bar_y, bar_x + bar_w, bar_y + bar_h, m_bar_paint);
					m_canvas.restore();
				}
			}
			
			if(start_show_arrow)
			{
				// draw blue arrow 
				if(moving_time == 0)
					moving_time = System.currentTimeMillis();
				
				float percent = (System.currentTimeMillis() - moving_time) / 1000.0f;
				//			Bitmap head = m_head1;
				if(percent > 1)
				{
					m_shouldExit = true;
					moving_time = 0;
					//				head = m_head2;
					percent = 1;
					goNextSceneHandler.sendEmptyMessageDelayed(0, 5000);
				}
				
				m_canvas.save();
				m_canvas.scale(1, percent, (Utils.m_screenHeight - m_img_support_factory.getWidth()) / 2.0f, 480 * Utils.m_screenScaleX);
				m_canvas.drawBitmap(m_img_support_factory, 
						(Utils.m_screenHeight - m_img_support_factory.getWidth()) / 2.0f, 
						470 * Utils.m_screenScaleX - m_img_support_factory.getHeight(), m_paint);
				m_canvas.restore();
			}
			
			m_canvas.drawBitmap(title_weight, (Utils.m_screenHeight - title_weight.getWidth()) / 2.0f, 286 * Utils.m_screenScaleX, m_paint);
			m_canvas.drawBitmap(title_support, (Utils.m_screenHeight - title_support.getWidth()) / 2.0f, 426 * Utils.m_screenScaleX, m_paint);
			
			if(m_btn_next_round != null)
			{
				RectF r = new RectF();
				r.left = Utils.m_screenHeight - 10 * Utils.m_screenScaleY - m_btn_next_round.getWidth();
				r.top = 10 * Utils.m_screenScaleX;
				r.right = r.left + m_btn_next_round.getWidth();
				r.bottom = r.top + m_btn_next_round.getHeight();
				m_btn_next_round.setBounds(r);
				m_btn_next_round.drawSprite(m_canvas, m_paint);
			}
			
		}
		
		private Handler goNextSceneHandler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub
				
				ShareData.Activity_Navigator = true;
				
				((Scene_fourth)m_context).finish();
				
				Intent intent = new Intent(m_context, SupportFactor.class);
				m_context.startActivity(intent);
							
				return false;
			}
		});
	}	

}
