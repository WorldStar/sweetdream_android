package com.tiseno.sweetdream.movie;

import com.tiseno.sweetdream.SDButtonSprite;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.sound.CSound;
import com.tiseno.sweetdream.supportfactor.SupportFactor;

import android.R.color;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class Scene_third_view extends SurfaceView implements SurfaceHolder.Callback {

	private Context m_context = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	private Bitmap m_img_bg_default = null;
	private Bitmap m_head1 = null;
	private Bitmap m_head2 = null;
	
	private SDButtonSprite m_btn_next_round = null;
	
	public Scene_third_view(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		loadResource();
		
		m_holder = getHolder();
		m_holder.setKeepScreenOn(false);
		m_holder.addCallback(this);
		
		this.setOnTouchListener(m_touchListener);
		
	}
	
	private OnTouchListener m_touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			
			if(m_drawThread.m_shouldExit)
				return true;
			
			float x = event.getX();
			float y = event.getY();
			
			switch(event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					if(m_btn_next_round.contains(x, y))
					{
						m_btn_next_round.setTouchDown(x, y);
					}
					break;
				}
				case MotionEvent.ACTION_MOVE:
				{
					m_btn_next_round.setTouchMove(x, y);
					break;
				}
				case MotionEvent.ACTION_UP:
				{
					if(m_btn_next_round.contains(x, y))
					{
						m_btn_next_round.setTouchUp(x, y);
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					}
					break;
				}
			}
			
			return true;
		}
	};

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {		// TODO Auto-generated method stub
		
		m_drawThread = new DrawThread();
		m_drawThread.setDaemon(true);
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		m_drawThread.m_shouldExit = true;
		
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResources();
	}
	
	private void loadResource()
	{
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapYX(m_context, "img/bg_scene_3.png");
		
		if(m_head1 == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
			{
				m_head1 = Utils.loadScreenScaledBitmapYX(m_context, "img/head_1.png");
			}else if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				m_head1 = Utils.loadScreenScaledBitmapYX(m_context, "img/fhead_1.png");
			}
		}
		
		if(m_head2 == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
			{
				m_head2 = Utils.loadScreenScaledBitmapYX(m_context, "img/head_2.png");
			}else if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				m_head2 = Utils.loadScreenScaledBitmapYX(m_context, "img/fhead_2.png");
			}
		}
		
		if(m_btn_next_round == null)
		{
			m_btn_next_round = SDButtonSprite.createFromAsset(m_context, "img/btn_next_round.png");
			m_btn_next_round.setNavigateHandler(mNextRoundHandler);
		}
	}
	
	private void removeResources()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;			
		}
		
		if(m_head1 != null)
		{
			m_head1.recycle();
			m_head1 = null;
		}
		
		if(m_head2 != null)
		{
			m_head2.recycle();
			m_head2 = null;
		}
		
		if(m_btn_next_round != null)
		{
			m_btn_next_round.destroy();
			m_btn_next_round = null;
		}
	}
	
	private Handler mNextRoundHandler = new Handler(new Handler.Callback() {		
		@Override
		public boolean handleMessage(Message msg) {
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
			// TODO Auto-generated method stub
			((Scene_third)m_context).finish();
			Intent intent = new Intent(m_context, SupportFactor.class);
			m_context.startActivity(intent);
			return false;
		}
	});
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		
		private long moving_time = 0;
		
		public void run(){
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						m_canvas.drawColor(color.black);
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
				try {
					sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		public void drawEngine()
		{			
			loadResource();
			drawResource();
		}		
		
		private void drawResource()
		{
			//draw bg
			m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			if(moving_time == 0)
				moving_time = System.currentTimeMillis();
			
			float total_dist = 100;
						
			float percent = (System.currentTimeMillis() - moving_time) / 3000.0f;
			float dist = -(percent * total_dist);
			
			Bitmap head = m_head1;
			if(percent > 1)
			{
				m_shouldExit = true;
				moving_time = 0;
				dist = -total_dist;
				head = m_head2;
				goNextSceneHandler.sendEmptyMessageDelayed(0, 60);
			}
			
			m_canvas.save();
			m_canvas.translate(0, dist);

			m_canvas.drawBitmap(head, (Utils.m_screenHeight - m_head1.getWidth()) / 2.0f, Utils.m_screenWidth - m_head1.getHeight() + total_dist, m_paint);
			m_canvas.restore();
			
			if(m_btn_next_round != null)
			{
				RectF r = new RectF();
				r.left = Utils.m_screenHeight - 10 * Utils.m_screenScaleY - m_btn_next_round.getWidth();
				r.top = 10 * Utils.m_screenScaleX;
				r.right = r.left + m_btn_next_round.getWidth();
				r.bottom = r.top + m_btn_next_round.getHeight();
				m_btn_next_round.setBounds(r);
				m_btn_next_round.drawSprite(m_canvas, m_paint);
			}
		}
		
		private Handler goNextSceneHandler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub			
				
				ShareData.Activity_Navigator = true;
				
				((Scene_third)m_context).finish();
				
				Intent intent = new Intent(m_context, Scene_fourth.class);
				m_context.startActivity(intent);				
							
				return true;
			}
		});
	}	

}
