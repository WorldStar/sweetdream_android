package com.tiseno.sweetdream;

public class ShareData {
	
	public static boolean Activity_Navigator = false;

	public static final int Gender_Mail = 100;
	public static final int Gender_Femail = 101;
	
	public static int BMI_UNDER = 100;
	public static int BMI_NORMAL_1 = 101;
	public static int BMI_NORMAL_2 = 102;
	public static int BMI_NORMAL_3 = 103;
	public static int BMI_OVER_1 = 104;
	public static int BMI_OVER_2 = 105;
	public static int BMI_OBESE_1 = 106;
	public static int BMI_OBESE_2 = 107;
	public static int BMI_OBESE_3 = 108;
	
	public static int BMI_STATUS = 100;
	
	public static int SLEEPING_POSITION = -1;
	
	public static String SLEEPING_POSITION_STRING = "";
	public static String FACTOR_STRING = "";
	public static String RECOMMENDED_PILLOW = "";
	public static float SUPPORTED_FACTOR_VALUE = 0;
	
	public static String BMI_IMAGE_PATH = "";
	
	
	public static int Selected_Gender = Gender_Mail;
	public static int Selected_Weight = -1;
	public static int Selected_Height = -1;
	
	public static float BMI_VALUE = 0;
	
	public static void doCalc() {
		// TODO Auto-generated method stub
		
		if(Selected_Gender == Gender_Mail)
		{
			BMI_IMAGE_PATH = "img/male_";
		}else
		{
			BMI_IMAGE_PATH = "img/female_";
		}
		
		
		float weight = Selected_Weight;
		float height = Selected_Height /100.0f;
		height = (height == 0) ? 1 : height;
		Float bmi = weight  / (height*height);
		BMI_VALUE = bmi.isNaN()?0:bmi;
		
		if(0 <= BMI_VALUE && BMI_VALUE < 19)
		{
			BMI_STATUS = BMI_UNDER;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS";
					SUPPORTED_FACTOR_VALUE = 2.8f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow S";
					SUPPORTED_FACTOR_VALUE = 3.0f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS";
					SUPPORTED_FACTOR_VALUE = 2.8f;
					break;
				}
			}
					
			FACTOR_STRING = "Under Weight";			
			BMI_IMAGE_PATH += "bmi_underweight.png";
			
		}else if(19 <= BMI_VALUE && BMI_VALUE < 20.5f)
		{
			BMI_STATUS = BMI_NORMAL_1;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow S";
					SUPPORTED_FACTOR_VALUE = 3.2f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow S/M";
					SUPPORTED_FACTOR_VALUE = 3.4f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS";
					SUPPORTED_FACTOR_VALUE = 3.2f;
					break;
				}
			}
					
			FACTOR_STRING = "Normal";
			BMI_IMAGE_PATH += "bmi_normal.png";
		}else if(20.5 <= BMI_VALUE && BMI_VALUE < 22.5f)
		{
			BMI_STATUS = BMI_NORMAL_2;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow S/M";
					SUPPORTED_FACTOR_VALUE = 3.5f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow S/M";
					SUPPORTED_FACTOR_VALUE = 3.6f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS/S";
					SUPPORTED_FACTOR_VALUE = 3.5f;
					break;
				}
			}
					
			FACTOR_STRING = "Normal";
			BMI_IMAGE_PATH += "bmi_normal.png";
		}else if(22.5 <= BMI_VALUE && BMI_VALUE < 25.0f)
		{
			BMI_STATUS = BMI_NORMAL_3;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow S/M";
					SUPPORTED_FACTOR_VALUE = 3.7f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow M/L";
					SUPPORTED_FACTOR_VALUE = 3.8f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS/S";
					SUPPORTED_FACTOR_VALUE = 3.7f;
					break;
				}
			}
					
			FACTOR_STRING = "Normal";
			BMI_IMAGE_PATH += "bmi_normal.png";
		}else if(25.0f <= BMI_VALUE && BMI_VALUE < 27.5f)
		{
			BMI_STATUS = BMI_OVER_1;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow M/L";
					SUPPORTED_FACTOR_VALUE = 4.0f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow L";
					SUPPORTED_FACTOR_VALUE = 4.2f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS/S";
					SUPPORTED_FACTOR_VALUE = 4.1f;
					break;
				}
			}
					
			FACTOR_STRING = "Over Weight";
			BMI_IMAGE_PATH += "bmi_overweight.png";
		}else if(27.5f <= BMI_VALUE && BMI_VALUE < 30.0f)
		{
			BMI_STATUS = BMI_OVER_2;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow L";
					SUPPORTED_FACTOR_VALUE = 4.5f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow L";
					SUPPORTED_FACTOR_VALUE = 4.8f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XS/S";
					SUPPORTED_FACTOR_VALUE = 4.6f;
					break;
				}
			}
					
			FACTOR_STRING = "Over Weight";
			BMI_IMAGE_PATH += "bmi_overweight.png";
		}else if(30.0f <= BMI_VALUE && BMI_VALUE < 35.0f)
		{
			BMI_STATUS = BMI_OBESE_1;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow L";
					SUPPORTED_FACTOR_VALUE = 5.2f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow XL";
					SUPPORTED_FACTOR_VALUE = 5.5f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow S";
					SUPPORTED_FACTOR_VALUE = 5.3f;
					break;
				}
			}
					
			FACTOR_STRING = "Obese";
			BMI_IMAGE_PATH += "bmi_obese.png";
		}else if(35.0f <= BMI_VALUE && BMI_VALUE < 40.0f)
		{
			BMI_STATUS = BMI_OBESE_2;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow L";
					SUPPORTED_FACTOR_VALUE = 5.8f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow XL";
					SUPPORTED_FACTOR_VALUE = 6.0f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow XL";
					SUPPORTED_FACTOR_VALUE = 5.9f;
					break;
				}
			}
					
			FACTOR_STRING = "Obese";
			BMI_IMAGE_PATH += "bmi_obese.png";
		}else if(40.0f <= BMI_VALUE && BMI_VALUE < 4999940.0f)
		{
			BMI_STATUS = BMI_OBESE_3;
			
			switch(SLEEPING_POSITION)
			{
				case 0:
				{
					SLEEPING_POSITION_STRING = "Supine Posture (back sleeper)";
					RECOMMENDED_PILLOW = "Pillow L";
					SUPPORTED_FACTOR_VALUE = 6.2f;
					break;
				}
				case 1:
				{
					SLEEPING_POSITION_STRING = "Lateral Posture (side sleeper)";
					RECOMMENDED_PILLOW = "Pillow XL";
					SUPPORTED_FACTOR_VALUE = 6.5f;
					break;
				}
				case 2:
				{
					SLEEPING_POSITION_STRING = "Prone Posture (stomach sleeper)";
					RECOMMENDED_PILLOW = "Pillow S";
					SUPPORTED_FACTOR_VALUE = 6.3f;
					break;
				}
			}
					
			FACTOR_STRING = "Obese";
			BMI_IMAGE_PATH += "bmi_obese.png";
		}
		
	}
}
