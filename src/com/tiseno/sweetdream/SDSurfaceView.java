package com.tiseno.sweetdream;

import com.tiseno.sweetdream.sleepposition.SDSleepPosition;
import com.tiseno.sweetdream.sound.CSound;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.GestureDetector.OnGestureListener;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class SDSurfaceView extends SurfaceView implements Callback {

	private Context m_context = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	// image resources
	// for logo image
	private Bitmap m_img_bg_default = null;
	//for start Recovery
	private Bitmap m_img_bar_top = null;

	//for select gender
	private SDChooseGenderBoySprite m_gender_boy = null;
	private SDChooseGenderGirlSprite m_gender_girl = null;
	private SDClock m_clock = null;
	private SDTelephone m_telephone = null;
	private SDBottomText m_gender_bottom_text= null;
	
	private SDRightArrow m_right_arrow = null;
	private SDLeftArrow m_left_arrow = null;
	
	private SDButtonSprite m_prev_scene = null;
	private SDButtonSprite m_next_scene = null;
	
	private GestureDetector m_gestureDetector = null; 
	
	private OnGestureListener mGestureListner = new OnGestureListener() {
		
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			float x = e.getX();
			float y = e.getY();
			
			if(m_telephone.contains(x, y))
			{
				m_telephone.setTouchDown(x, y);
				CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
			}
			
			if(m_right_arrow.contains(x, y))
			{
				if(!m_gender_boy.isMovingAnimation() && !m_gender_girl.isMovingAnimation())
				{
					m_gender_boy.startMoveLeft();
					m_gender_girl.startMoveLeft();
					m_right_arrow.setVisible(false);
					m_left_arrow.setVisible(true);
					
					ShareData.Selected_Gender = ShareData.Gender_Femail;
					
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
				}
			}
			
			if(m_left_arrow.contains(x, y))
			{
				if(!m_gender_boy.isMovingAnimation()  && !m_gender_girl.isMovingAnimation())
				{
					m_gender_boy.startMoveRight();
					m_gender_girl.startMoveRight();
					m_right_arrow.setVisible(true);
					m_left_arrow.setVisible(false);
					
					ShareData.Selected_Gender = ShareData.Gender_Mail;
					
					CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
				}

			}
			
			if(m_gender_boy.contains(x, y))
			{
				m_gender_boy.setTouchDown(x, y);
				CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
			}
			
			if(m_gender_girl.contains(x, y))
			{
				m_gender_girl.setTouchDown(x, y);
				CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
			}
			
			if(m_next_scene.contains(x, y))
			{
				m_next_scene.setTouchDown(x, y);
				m_next_scene.setTouchUp(x, y);
				
				CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
			}
			return false;
		}
		
		@Override
		public void onShowPress(MotionEvent e) {
			// TODO Auto-generated method stub		

		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
				float distanceY) {
			// TODO Auto-generated method stub
			
			return false;
		}
		
		@Override
		public void onLongPress(MotionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			// TODO Auto-generated method stub
			
			if((e2.getX() - e1.getX()) > 0)
			{
				if(ShareData.Selected_Gender == ShareData.Gender_Femail)
				{
					if(!m_gender_boy.isMovingAnimation() && !m_gender_girl.isMovingAnimation())
					{
						m_gender_boy.startMoveRight();
						m_gender_girl.startMoveRight();
						m_right_arrow.setVisible(true);
						m_left_arrow.setVisible(false);
						
						ShareData.Selected_Gender = ShareData.Gender_Mail;	
						
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					}

				}

			}else
			{				
				if(ShareData.Selected_Gender == ShareData.Gender_Mail )
				{
					if(!m_gender_boy.isMovingAnimation() && !m_gender_girl.isMovingAnimation())
					{
						m_gender_boy.startMoveLeft();
						m_gender_girl.startMoveLeft();
						m_right_arrow.setVisible(false);
						m_left_arrow.setVisible(true);
						
						ShareData.Selected_Gender = ShareData.Gender_Femail;
						
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
					}

				}	
			}
			return false;
		}
		

		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
						
			return false;
		}	
	};
	
	private Handler goNextScene = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;
			((SDMainActivity)m_context).finish();
			Intent intent = new Intent(m_context, SDSleepPosition.class);
			m_context.startActivity(intent);
			
			return false;
		}
	});
	
	public boolean touchEvent(MotionEvent event)
	{
		boolean returnVal = m_gestureDetector.onTouchEvent(event);		
		return returnVal;
	}
	
	public SDSurfaceView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		loadSelectGender();
	
		m_holder = getHolder();
		m_holder.addCallback(this);
		
		m_gestureDetector = new GestureDetector(context, mGestureListner);
		m_gestureDetector.setIsLongpressEnabled(false);
	}
	
	private void loadSelectGender()
	{
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapXY(m_context, "img/bg_default.png");
		
		if(m_img_bar_top == null)
			m_img_bar_top = Utils.loadScreenScaledBitmapXY(m_context, "img/bar_top.png");
					
		if(m_right_arrow == null)
		{
			m_right_arrow = new SDRightArrow(m_context);				
		}
		
		if(m_left_arrow == null)
		{
			m_left_arrow = new SDLeftArrow(m_context);
		}
		
		if(m_gender_boy == null)
		{
			m_gender_boy= new SDChooseGenderBoySprite(m_context);
			m_gender_boy.addChooseGenderHandler(changeGenderHandler);
			if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				m_gender_boy.startMoveLeft();
				m_right_arrow.setVisible(false);
				m_left_arrow.setVisible(true);
			}
		}
		if(m_gender_girl == null)
		{
			m_gender_girl = new SDChooseGenderGirlSprite(m_context);
			m_gender_girl.addChooseGenderHandler(changeGenderHandler);
			if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				m_gender_girl.startMoveLeft();
				m_right_arrow.setVisible(false);
				m_left_arrow.setVisible(true);
			}
		}
		
		if(m_clock == null)
			m_clock = new SDClock(m_context);
		
		if(m_telephone == null)
		{
			m_telephone = new SDTelephone(m_context);
			m_telephone.addClickHandler(callPhoneHandler);
		}
		
		if(m_gender_bottom_text == null)
		{
			m_gender_bottom_text = new SDBottomText(m_context, "Tap me to select gender");
		}
		
		if(m_prev_scene == null)
		{
			m_prev_scene = SDButtonSprite.createFromAsset(m_context, "img/btn_prev.png");
			RectF rect = new RectF();
			rect.left = 6 * Utils.m_screenScaleX;
			rect.top = 6* Utils.m_screenScaleY;
			rect.right = rect.left + m_prev_scene.getWidth();
			rect.bottom = rect.top + m_prev_scene.getHeight();
			m_prev_scene.setBounds(rect);
			
			m_prev_scene.setEnable(false);
		}
		
		if(m_next_scene == null)
		{
			m_next_scene = SDButtonSprite.createFromAsset(m_context, "img/btn_next.png");
			RectF rect = new RectF();
			rect.left = Utils.m_screenWidth - 6 * Utils.m_screenScaleX - m_next_scene.getWidth();
			rect.top = 6* Utils.m_screenScaleY;
			rect.right = rect.left + m_next_scene.getWidth();
			rect.bottom = rect.top + m_next_scene.getHeight();
			m_next_scene.setBounds(rect);
			
			if(ShareData.SLEEPING_POSITION != -1)
			{
				m_next_scene.setEnable(true);
				m_next_scene.setNavigateHandler(goNextScene );
			}else
				m_next_scene.setEnable(false);
		}
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.setDaemon(true);
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
		m_drawThread.m_shouldExit = true;
				
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResource();
		
	}
	
	private void removeResource()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
			
		}
		if(m_img_bar_top != null)
		{
			m_img_bar_top.recycle();
			m_img_bar_top = null;
		}
			
		if(m_gender_boy != null)
		{
			m_gender_boy.removeResource();
			m_gender_boy = null;
		}
		if(m_gender_girl != null)
		{
			m_gender_girl.removeResource();
			m_gender_girl = null;
		}
		
		if(m_clock != null)
		{
			m_clock.removeResource();
			m_clock = null;
		}
		
		if(m_telephone != null)
		{
			m_telephone.removeResource();
			m_telephone = null;
		}
		
		if(m_right_arrow != null)
		{
			m_right_arrow.destroy();
			m_right_arrow = null;
		}
		
		if(m_left_arrow != null)
		{
			m_left_arrow.destroy();
			m_left_arrow = null;
		}
		
		if(m_gender_bottom_text != null)
		{
			m_gender_bottom_text = null;
		}
		
		if(m_prev_scene != null)
		{
			m_prev_scene.destroy();
			m_prev_scene = null;
		}
		
		if(m_next_scene != null)
		{
			m_next_scene.destroy();
			m_next_scene = null;
		}
	}
	
	private Handler changeGenderHandler = new Handler(new Handler.Callback() {			
		@Override
		public boolean handleMessage(Message msg) {
			m_drawThread.m_shouldExit = true;
			
			// TODO Auto-generated method stub				
			((SDMainActivity)m_context).overridePendingTransition(0, R.anim.exitanim);
			
			ShareData.Activity_Navigator = true;
			
			((SDMainActivity)m_context).finish();
			Intent intent = new Intent(m_context, SDSleepPosition.class);
			m_context.startActivity(intent);				
			return true;
		}
	});
	
	private Handler callPhoneHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
//			
			new AlertDialog.Builder(m_context).setTitle("Customer Care Hotline")
			.setMessage("1300-88-9921")
			.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			})
			.setNegativeButton("Call", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Uri number = Uri.parse("tel:1300889921");
					Intent intent = new Intent(Intent.ACTION_CALL, number);
					m_context.startActivity(intent);
				}
			} ).show();
							
			return false;
		}
	});	
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		
		public void run(){
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
				try {
					sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				

			}
		}
		
		public void drawEngine()
		{
			loadSelectGender();
			drawSelectGender();
		}				
		
		private void drawSelectGender()
		{
			//draw bg
			m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			// draw top bar
			m_canvas.drawBitmap(m_img_bar_top, 0, 0, m_paint);
			
			// draw clock
			m_clock.drawCanvas(m_canvas, m_paint);
			
			// draw telephone
			m_telephone.DrawCanvas(m_canvas, m_paint);
			
			// draw gender boy
			m_gender_boy.drawCanvas(m_canvas, m_paint);
			
			//draw gender girl
			m_gender_girl.drawCanvas(m_canvas, m_paint);
			
			//draw right arrow
			m_right_arrow.drawCanvas(m_canvas, m_paint);
			
			//draw left arrow
			m_left_arrow.drawCanvas(m_canvas, m_paint);
			
			//draw bottom text
			m_gender_bottom_text.drawText(m_canvas, 1);
			
			//draw navigator button
			m_prev_scene.drawSprite(m_canvas, m_paint);
			m_next_scene.drawSprite(m_canvas, m_paint);
			
		}		

	}
	
}
