package com.tiseno.sweetdream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Align;
import android.graphics.Typeface;

public class SDIntroPageSprite {
	private Bitmap m_bmp = null;
	private RectF m_bound = null;
	
	private Paint m_filterPaint = null;
	private Paint font_paint = null;
	
	private final int TOUCH_NULL = 0;
	private final int TOUCH_DOWN = 1;
	private final int TOUCH_MOVE = 2;
	private int m_touchStatus = TOUCH_NULL;
	private float m_touched_y = -1;
	
	private float m_distance_y = 0;
	
	private boolean is_restoreStart = false;
	
	private int stepper = 30;
	
	private SDIntroPageSprite(Context context, Bitmap bmp)
	{
		m_bmp = bmp;
		m_bound = null;
		m_touchStatus = TOUCH_NULL;
		
		m_filterPaint = new Paint();
		m_filterPaint.setDither(true);
		m_filterPaint.setAntiAlias(true);
		m_filterPaint.setColorFilter(new LightingColorFilter(0x666666, 0x000000));
		
		font_paint = new Paint();
		font_paint.setDither(true);
		font_paint.setAntiAlias(true);
		font_paint.setTextSize(25 * Utils.m_screenScaleY);
		font_paint.setTextAlign(Align.CENTER);
		font_paint.setColor(Color.BLACK);
		font_paint.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/CreteRound-Regular.otf"));

	}
	
	public static SDIntroPageSprite createFromAsset(Context context, String img_path)
	{
		Bitmap bmp = Utils.loadScreenScaledBitmapY(context, img_path);
		
		SDIntroPageSprite sprite = new SDIntroPageSprite(context, bmp);
		
		return sprite;
	}
	
	public RectF getBounds()
	{
		return m_bound;
	}
	
	public void setBounds(RectF rect)
	{
		m_bound = rect;
	}
	
	public void drawSprite(Canvas canvas, Paint paint)
	{
		if(m_bmp != null && m_bound != null && !m_bound.isEmpty())
		{
			if(is_restoreStart && m_touchStatus == TOUCH_NULL)
			{
				stepper += 10;
				m_distance_y += (stepper * Utils.m_screenScaleY);
				 
				float y = m_bound.top + m_distance_y;
				if(m_distance_y > (40 * Utils.m_screenScaleY))
				{
					y = m_bound.top + 20 * Utils.m_screenScaleY;
					is_restoreStart = false;
				}
				canvas.drawBitmap(m_bmp, m_bound.left, y, paint);
				
				float txt_y = y + m_bound.height() - 80 * Utils.m_screenScaleY;
				canvas.drawText("Optimise brd properties to archieve maximal", Utils.m_screenWidth/ 2.0f, txt_y, font_paint);
				txt_y = txt_y + 30 * Utils.m_screenScaleY;
				canvas.drawText("recovery during sleep", Utils.m_screenWidth/ 2.0f, txt_y, font_paint);
				
			}else{
				stepper = 30;
				float y = m_bound.top + m_distance_y;
				if(m_distance_y > (20 * Utils.m_screenScaleY))
				{
					y = m_bound.top + 20 * Utils.m_screenScaleY;
				}
				canvas.drawBitmap(m_bmp, m_bound.left, y, paint);
				float txt_y = y + m_bound.height() - 90 * Utils.m_screenScaleY;
				canvas.drawText("Optimise bed properties to archieve maximal", Utils.m_screenWidth/ 2.0f, txt_y, font_paint);
				txt_y = txt_y + 30 * Utils.m_screenScaleY;
				canvas.drawText("recovery during sleep", Utils.m_screenWidth/ 2.0f, txt_y, font_paint);
			}

		}
	}
	
	public boolean contains(float x, float y)
	{
		boolean isContained = false;
		if(m_bound == null || m_bound.isEmpty())
			return isContained;
		
		isContained = m_bound.contains(x, y);
		return isContained;
	}
	
	public float getWidth()
	{
		float w = 0;
		if(m_bmp != null)
		{
			w = m_bmp.getWidth();
		}
		
		return w;
	}
	
	public float getHeight()
	{
		float h = 0;
		if(m_bmp != null)
		{
			h = m_bmp.getHeight();
		}
		
		return h;
	}
	
	public void destroy()
	{
		if(m_bmp != null)
		{
			m_bmp.recycle();
			m_bmp = null;
		}
		
		m_bound = null;
	}

	public void setTouchDown(float x, float y) {
		// TODO Auto-generated method stub
		if(is_restoreStart)
			return;
		
		m_touchStatus = TOUCH_DOWN;
		m_touched_y = y;		
	}

	public void setTouchMove(float x, float y) {
//		 TODO Auto-generated method stub
		if(is_restoreStart)
			return;
		
		if(m_touchStatus != TOUCH_NULL)
		{
			boolean contain = this.contains(x, y);
			if(!contain)
			{
				if(!is_restoreStart)
				{
					m_touchStatus = TOUCH_NULL;
					m_touched_y = -1;
//					m_distance_y = 0;
					is_restoreStart = true;
				}
				return;
			}
			
			m_touchStatus = TOUCH_MOVE;
			m_distance_y = y - m_touched_y;
		}
	}

	public void setTouchUp(float x, float y) {
		// TODO Auto-generated method stub
		if(is_restoreStart)
			return;
		
		if(m_touchStatus == TOUCH_DOWN || m_touchStatus == TOUCH_MOVE)
		{
			if(!is_restoreStart)
			{
				m_touchStatus = TOUCH_NULL;
				m_touched_y = -1;
	//			m_distance_y = 0;
				is_restoreStart = true;
			}
		}
		
	}
}
