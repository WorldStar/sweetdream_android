package com.tiseno.sweetdream.measure;

import com.tiseno.sweetdream.R;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.sound.CSound;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;

public class MeasureHeightActivity extends Activity {
	private MeasureHeightView surfaceView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		surfaceView = new MeasureWeightView(this);
		setContentView(R.layout.measure_height_linear);
		
		overridePendingTransition(R.anim.enteranim, R.anim.exitanim);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.play();
		}else
		{
			ShareData.Activity_Navigator = false;
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.pause();
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return super.onTouchEvent(event);
	}
	
	public void onClickWeightDone(View v){
		surfaceView = (MeasureHeightView)findViewById(R.id.measure_height_view);
		surfaceView.dissmissPopup();
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
		{
			final boolean bExit = false;
			
			new AlertDialog.Builder(this).setTitle("SweetDream")
			.setMessage("Do you want to close this app?")
			.setPositiveButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			})
			.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					CSound.sharedSoundController().stopMusic();
					MeasureHeightActivity.this.finish();
				}
			} ).show();
			
			return true;
		}
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_HOME)
		{
			CSound.sharedSoundController().stopMusic();
			return true;
		}
		
		return false;
	}
}
