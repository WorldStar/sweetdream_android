package com.tiseno.sweetdream.measure;

import com.quietlycoding.android.picker.NumberPicker;
import com.tiseno.sweetdream.R;
import com.tiseno.sweetdream.SDBottomText;
import com.tiseno.sweetdream.SDButtonSprite;
import com.tiseno.sweetdream.SDChooseGenderBoySprite;
import com.tiseno.sweetdream.SDChooseGenderGirlSprite;
import com.tiseno.sweetdream.SDClock;
import com.tiseno.sweetdream.SDTelephone;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.sleepposition.SDSleepPosition;
import com.tiseno.sweetdream.sound.CSound;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

public class MeasureWeightView extends SurfaceView implements SurfaceHolder.Callback {

	private Context m_context = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	// image resources
	// for logo image
	private Bitmap m_img_bg_default = null;
	//for start Recovery
	private Bitmap m_img_bar_top = null;
	//for select gender
	private SDChooseGenderBoySprite m_gender_boy = null;
	private SDChooseGenderGirlSprite m_gender_girl = null;
	private SDClock m_clock = null;
	private SDTelephone m_telephone = null;
	private SDBottomText m_gender_bottom_text= null;
	
	private Bitmap body_skinny = null;
	private Bitmap body_fat = null;
	private Bitmap body_normal = null;
	
	PopupWindow popup = null;
	View popupView = null;
	LinearLayout linear = null;
	protected boolean notouch = false;
	
	private boolean enterd_weight = false;
	
	private SDButtonSprite m_prev_scene = null;
	private SDButtonSprite m_next_scene = null;
	
	private long moving_time = 0;
	
	private boolean enableTouch = false;
	
	//touch listener
	private OnTouchListener m_touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if(popup != null || enterd_weight == true)
				return false;
			
			if(!enableTouch)
				return true;
			
			switch(event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					if(m_prev_scene != null)
					{
						if(m_prev_scene.contains(event.getX(), event.getY()))
						{
							m_prev_scene.setTouchDown(event.getX(), event.getY());
							m_prev_scene.setTouchUp(event.getX(), event.getY());
							CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
							break;
						}
					}
					
					if(m_next_scene != null)
					{
						if(m_next_scene.contains(event.getX(), event.getY()))
						{
							m_next_scene.setTouchDown(event.getX(), event.getY());
							m_next_scene.setTouchUp(event.getX(), event.getY());
							CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
							break;
						}
					}
					
					if(m_gender_boy.contains(event.getX(), event.getY()) || m_gender_girl.contains(event.getX(), event.getY()))
					{
						CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
						
						linear = (LinearLayout) ((MeasureWeightActivity)m_context).findViewById(R.id.measure_weight_linear_layout);
						popupView = View.inflate(m_context, R.layout.weight_popup, null);
						popupView.setClickable(true);
						NumberPicker np = (NumberPicker) popupView.findViewById(R.id.numberPicker1);
					    String[] nums = new String[5];
					    for(int i=0; i<nums.length; i++)
					           nums[i] = Integer.toString(i);
					    
					    np.setRange(0, 4);
					    int val = ShareData.Selected_Weight / 100;
					    np.setCurrent(val);
					    
						np = (NumberPicker) popupView.findViewById(R.id.numberPicker2);
					    nums = new String[10];
					    for(int i=0; i<nums.length; i++)
					           nums[i] = Integer.toString(i);

					    np.setRange(0, 9);
					    val = (ShareData.Selected_Weight %100) / 10;
					    np.setCurrent(val);
					    
						np = (NumberPicker) popupView.findViewById(R.id.numberPicker3);
					    nums = new String[10];
					    for(int i=0; i<nums.length; i++)
					           nums[i] = Integer.toString(i);

					    np.setRange(0, 9);
					    val = (ShareData.Selected_Weight % 10);
					    np.setCurrent(val);
					    
					    EditText edit = (EditText)popupView.findViewById(R.id.txtview_unit);
						edit.setText("KG");
						
						popup = new PopupWindow(popupView, Utils.m_screenWidth, Utils.m_screenHeight);
						popup.setIgnoreCheekPress();
						popup.showAtLocation(linear, Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM, 0, 0);					
						popup.setAnimationStyle(-1);
					}
					
					break;
				}
			}
			
			return true;
		}
	};
	
	public MeasureWeightView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
//		m_context = context;
//		
//		loadSelectGender();
//		
//		m_holder = getHolder();
//		m_holder.addCallback(this);
//		
//		setOnTouchListener(m_touchListener);
	}
	
	public MeasureWeightView(Context context, AttributeSet set)
	{
		super(context, set);	
		
		// TODO Auto-generated constructor stub
		m_context = context;
		
		moving_time = 0;
		
		if(ShareData.Selected_Weight ==-1)
			ShareData.Selected_Weight = 0;
			
		
		loadSelectGender();
		
		m_holder = getHolder();
		m_holder.addCallback(this);
		
		enableTouch = false;
		
		setOnTouchListener(m_touchListener);
		
		(new Handler()).postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				enableTouch = true;
			}
		}, 1500);		
		
	}
	
	public void dissmissPopup() {
		// TODO Auto-generated method stub
		if(popup != null)
		{
			NumberPicker np1 = (NumberPicker) popupView.findViewById(R.id.numberPicker1);
			int val_1 = np1.getCurrent();
			
			NumberPicker np2 = (NumberPicker) popupView.findViewById(R.id.numberPicker2);
			int val_2 = np2.getCurrent();
			
			NumberPicker np3 = (NumberPicker) popupView.findViewById(R.id.numberPicker3);
			int val_3 = np3.getCurrent();
			
			if(val_1 == 0 && val_2 == 0 && val_3 == 0)
			{
				AlertDialogManager alert = new AlertDialogManager();
				alert.showAlertDialog(m_context, "Weight Error", "Please enter your weight", false);
			}else
			{
				ShareData.Selected_Weight = val_1 * 100 + val_2 * 10 + val_3;
				popup.dismiss();
				popup = null;
				
				enterd_weight = true;
				goNextActHandler.sendEmptyMessageDelayed(0, 1500);
			}
				
		}
	}
	
	private Handler goNextActHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
						
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;

			
			((MeasureWeightActivity)m_context).overridePendingTransition(R.anim.enteranim, R.anim.exitanim);
			
			ShareData.Activity_Navigator = true;
			
			((MeasureWeightActivity)m_context).finish();
			
			Intent intent = new Intent(m_context, MeasureHeightActivity.class);
			m_context.startActivity(intent);
						
			return false;
		}
	});
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.setDaemon(true);
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		m_drawThread.m_shouldExit = true;
				
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		
		removeResource();
	}
	
	private Handler mPrevActHandler = new Handler(new Handler.Callback() {		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if(m_drawThread.m_shouldExit)
				return true;
			
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
            ((MeasureWeightActivity)m_context).finish();			
			
			Intent intent = new Intent(m_context, SDSleepPosition.class);
			
			intent.putExtra("backward", true);
			
			m_context.startActivity(intent);
			
			return true;
		}
	});
	
	private void loadSelectGender()
	{
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapXY(m_context, "img/bg_default.png");
		
		if(m_img_bar_top == null)
			m_img_bar_top = Utils.loadScreenScaledBitmapXY(m_context, "img/bar_top.png");
					
		if(m_gender_boy == null)
		{
			m_gender_boy= new SDChooseGenderBoySprite(m_context);
			m_gender_boy.startMoveRight();
		}
		if(m_gender_girl == null)
		{
			m_gender_girl = new SDChooseGenderGirlSprite(m_context);
			m_gender_girl.startMoveLeft();
		}
		
		if(m_telephone == null)
			m_telephone = new SDTelephone(m_context);
		
		if(m_clock == null)
			m_clock = new SDClock(m_context);
		
		if(m_gender_bottom_text == null)
		{
			m_gender_bottom_text = new SDBottomText(m_context, "Tap me to enter weight");
		}
		
		if(body_skinny == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
			{
				body_skinny = Utils.loadScreenScaledBitmapY(m_context, "img/male_skinny.png");
			}else if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				body_skinny = Utils.loadScreenScaledBitmapY(m_context, "img/female_skinny.png");
			}			
		}
		
		if(body_fat == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
			{
				body_fat = Utils.loadScreenScaledBitmapY(m_context, "img/male_fat.png");
			}else if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				body_fat = Utils.loadScreenScaledBitmapY(m_context, "img/female_fat.png");
			}
		}
		
		if(body_normal == null)
		{
			if(ShareData.Selected_Gender == ShareData.Gender_Mail)
			{
				body_normal = Utils.loadScreenScaledBitmapY(m_context, "img/male_normal.png");
			}else if(ShareData.Selected_Gender == ShareData.Gender_Femail)
			{
				body_normal = Utils.loadScreenScaledBitmapY(m_context, "img/female_normal.png");
			}
		}
		
		if(m_prev_scene == null)
		{
			m_prev_scene = SDButtonSprite.createFromAsset(m_context, "img/btn_prev.png");
			RectF rect = new RectF();
			rect.left = 6 * Utils.m_screenScaleY;
			rect.top = 6* Utils.m_screenScaleY;
			rect.right = rect.left + m_prev_scene.getWidth();
			rect.bottom = rect.top + m_prev_scene.getHeight();
			m_prev_scene.setBounds(rect);
			
			m_prev_scene.setEnable(true);
			m_prev_scene.setNavigateHandler(mPrevActHandler);
		}
		
		if(m_next_scene == null)
		{
			m_next_scene = SDButtonSprite.createFromAsset(m_context, "img/btn_next.png");
			RectF rect = new RectF();
			rect.left = Utils.m_screenWidth - 6 * Utils.m_screenScaleY - m_next_scene.getWidth();
			rect.top = 6* Utils.m_screenScaleY;
			rect.right = rect.left + m_next_scene.getWidth();
			rect.bottom = rect.top + m_next_scene.getHeight();
			m_next_scene.setBounds(rect);
			
			if(ShareData.Selected_Height > -1)
			{
				m_next_scene.setEnable(true);
				m_next_scene.setNavigateHandler(goNextActHandler);
			}else
				m_next_scene.setEnable(false);
			
		}
	}
	
	public void removeResource()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
		}
		
		
		if(m_img_bar_top != null)
		{
			m_img_bar_top.recycle();
			m_img_bar_top = null;
		}
					
		if(m_gender_boy != null)
		{				
			m_gender_boy.removeResource();
			m_gender_boy = null;
		}
		if(m_gender_girl != null)
		{
			m_gender_girl.removeResource();
			m_gender_girl = null;
		}
		
		if(m_telephone != null)
		{
			m_telephone.removeResource();
			m_telephone = null;
		}
		
		if(m_clock != null)
		{
			m_clock.removeResource();
			m_clock = null;
		}
		
		if(m_gender_bottom_text != null)
		{
			m_gender_bottom_text = null;
		}
		
		if(body_skinny != null)
		{
			body_skinny.recycle();
			body_skinny = null;
		}
		
		if(body_fat != null)
		{
			body_fat.recycle();
			body_fat = null;
		}
		
		if(body_normal != null)
		{
			body_normal.recycle();
			body_normal = null;
		}
		
		if(m_prev_scene != null)
		{
			m_prev_scene.destroy();
			m_prev_scene = null;
		}
		
		if(m_next_scene != null)
		{
			m_next_scene.destroy();
			m_next_scene = null;
		}
	}
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		
		public void run(){
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
//				long strtTime = System.currentTimeMillis();
				
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
				try {
					sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
			}
		}
		
		public void drawEngine()
		{			
			loadSelectGender();
			drawSelectGender();
		}	
		
		private void drawSelectGender()
		{
			//draw bg
			m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			// draw top bar
			m_canvas.drawBitmap(m_img_bar_top, 0, 0, m_paint);
			
			//draw navigator button
			m_prev_scene.drawSprite(m_canvas, m_paint);
			m_next_scene.drawSprite(m_canvas, m_paint);
			
			// draw clock
			m_clock.drawCanvas(m_canvas, m_paint);
			
			// draw telephone
			m_telephone.DrawCanvas(m_canvas, m_paint);
			
			if(!enterd_weight)
			{
				if(ShareData.Selected_Gender == ShareData.Gender_Mail)
				{
					// draw gender boy
					m_gender_boy.drawCanvas(m_canvas, m_paint);
				}else
				{
					//draw gender girl
					m_gender_girl.drawCanvas(m_canvas, m_paint);
				}
			}else
			{				
				if(moving_time == 0)
					moving_time = System.currentTimeMillis();
				
				float percent = (System.currentTimeMillis() - moving_time) / 500.0f;
				float diff = Utils.m_screenWidth * 0.65f - (percent * Utils.m_screenWidth * 0.65f);
				if(percent > 1)
				{
					diff = 0;
				}
						
				m_canvas.save();
				m_canvas.translate(diff, 0);
				if(ShareData.Selected_Weight < 50)
				{
					float x = (Utils.m_screenWidth - body_skinny.getWidth()) / 2.0f;
					float y = Utils.m_screenHeight - (SDChooseGenderBoySprite.body_bottom_y * Utils.m_screenScaleY + body_skinny.getHeight());
					m_canvas.drawBitmap(body_skinny, x, y, m_paint);
				}else if(ShareData.Selected_Weight > 80)
				{
					float x = (Utils.m_screenWidth - body_fat.getWidth()) / 2.0f;
					float y = Utils.m_screenHeight - (SDChooseGenderBoySprite.body_bottom_y * Utils.m_screenScaleY + body_fat.getHeight());
					m_canvas.drawBitmap(body_fat, x, y, m_paint);
				}else
				{
					float x = (Utils.m_screenWidth - body_normal.getWidth()) / 2.0f;
					float y = Utils.m_screenHeight - (SDChooseGenderBoySprite.body_bottom_y * Utils.m_screenScaleY + body_normal.getHeight());
					m_canvas.drawBitmap(body_normal, x, y, m_paint);
				}
				m_canvas.restore();
			}

			
			//draw bottom text
			m_gender_bottom_text.drawText(m_canvas, 1);
			
		}		

	}
}
