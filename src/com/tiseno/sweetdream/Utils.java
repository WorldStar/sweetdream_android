package com.tiseno.sweetdream;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Display;

public class Utils {
	public static final String LOGTAG = "Utils";

	public static int m_screenWidth;
	public static int m_screenHeight;

	public static float m_screenScaleX;
	public static float m_screenScaleY;

	public static float DEFAULT_WIDTH = 640;
	public static float DEFAULT_HEIGHT = 1136;

	private static long GC_DELAY_TIME = 100; // miliseconds

	public static void caculateScales(Activity ctxt) {
		Display display = ctxt.getWindowManager().getDefaultDisplay();
				
		m_screenWidth = display.getWidth();
		m_screenHeight = display.getHeight();
		
		m_screenScaleX = m_screenWidth / DEFAULT_WIDTH;
		m_screenScaleY = m_screenHeight / DEFAULT_HEIGHT;
	}
	
	public static void caculateScalesForLandscape(Activity ctxt) {
		// TODO Auto-generated method stub
		Display display = ctxt.getWindowManager().getDefaultDisplay();
		
		m_screenWidth = display.getHeight();
		m_screenHeight = display.getWidth();
		
		m_screenScaleX = m_screenWidth / DEFAULT_WIDTH;
		m_screenScaleY = m_screenHeight / DEFAULT_HEIGHT;
	}

	public static float screenScaleX() {
		return m_screenScaleX;
	}

	public static float screenScaleY() {
		return m_screenScaleY;
	}

	public static int getWorldX(float x) {
		return (int) (x * m_screenScaleX);
	}

	public static int getWorldY(float y) {
		return (int) (y * m_screenScaleY);
	}

	public static int getScreenX(float x) {
		return (int) (x / m_screenScaleX);
	}

	public static int getScreenY(float y) {
		return (int) (y / m_screenScaleY);
	}

	public static Bitmap loadScreenScaledBitmapXY(Context ctx, String fileName) {
		InputStream stream = null;
		try {
			stream = ctx.getResources().getAssets().open(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Bitmap bmp = BitmapFactory.decodeStream(stream);
		int dstW = (int) (bmp.getWidth() * m_screenScaleX) < 1 ? 1 : (int) (bmp
				.getWidth() * m_screenScaleX);
		int dstH = (int) (bmp.getHeight() * m_screenScaleY) < 1 ? 1
				: (int) (bmp.getHeight() * m_screenScaleY);

		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, dstW, dstH, true);
		bmp.recycle();
		bmp = null;

		try {
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scaledBitmap;
	}
	
	public static Bitmap loadScreenScaledBitmapYX(Context ctx, String fileName) {
		InputStream stream = null;
		try {
			stream = ctx.getResources().getAssets().open(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Bitmap bmp = BitmapFactory.decodeStream(stream);
		int dstW = (int) (bmp.getWidth() * m_screenScaleY) < 1 ? 1 : (int) (bmp
				.getWidth() * m_screenScaleY);
		int dstH = (int) (bmp.getHeight() * m_screenScaleX) < 1 ? 1
				: (int) (bmp.getHeight() * m_screenScaleX);

		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, dstW, dstH, true);
		bmp.recycle();
		bmp = null;

		try {
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scaledBitmap;
	}

	public static Bitmap loadScreenScaledBitmapX(Context ctx, String fileName) {
		// **/
		int count = 0;
		while (true) {
			count++;
			try {
				// /
				InputStream stream = null;
				try {
					stream = ctx.getResources().getAssets().open(fileName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Bitmap bmp = BitmapFactory.decodeStream(stream);

				if (m_screenScaleX == 1.0f) {
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return bmp;
				}

				int dstW = (int) (bmp.getWidth() * m_screenScaleX) < 1 ? 1
						: (int) (bmp.getWidth() * m_screenScaleX);
				int dstH = (int) (bmp.getHeight() * m_screenScaleX) < 1 ? 1
						: (int) (bmp.getHeight() * m_screenScaleX);
				Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, dstW,
						dstH, true);
				bmp.recycle();
				bmp = null;

				try {
					stream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return scaledBitmap;
				// **/
			} catch (OutOfMemoryError e) {
				Log.e("OutOfMemory",
						String.format("Try %d %s", count, fileName));
				System.gc();
				try {
					Thread.sleep(GC_DELAY_TIME);
				} catch (InterruptedException e1) {
					;
				}
			}

		}
		// //
	}

	public static Bitmap loadScreenScaledBitmapY(Context ctx, String fileName) {
		// **/
		int count = 0;
		while (count < 5) {
			count++;
			try {
				// /
				InputStream stream = null;
				try {
					stream = ctx.getResources().getAssets().open(fileName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.v("loadScreenScaledBitmapY", e.toString());
				}
				Bitmap bmp = BitmapFactory.decodeStream(stream);
				Log.v("loadBitmap", fileName);
				if (m_screenScaleY == 1.0f) {
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return bmp;
				}

				int dstW = (int) (bmp.getWidth() * m_screenScaleY) < 1 ? 1
						: (int) (bmp.getWidth() * m_screenScaleY);
				int dstH = (int) (bmp.getHeight() * m_screenScaleY) < 1 ? 1
						: (int) (bmp.getHeight() * m_screenScaleY);
				Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, dstW,
						dstH, true);
				bmp.recycle();
				bmp = null;

				try {
					stream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return scaledBitmap;
				// **/
			} catch (OutOfMemoryError e) {
				Log.e("OutOfMemory",
						String.format("Try %d %s", count, fileName));
				System.gc();
				try {
					Thread.sleep(GC_DELAY_TIME);
				} catch (InterruptedException e1) {
					;
				}
			}

		}
		// //
		return null;
	}

	public static Bitmap loadOriginalBitmap(Context ctx, String fileName) {
		InputStream stream = null;
		try {
			stream = ctx.getResources().getAssets().open(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.v("loadScreenScaledBitmapY", e.toString());
		}

		Bitmap bmp = null;
		try{
			bmp = BitmapFactory.decodeStream(stream);
		}catch (Exception e) {
			bmp = null;
		}

		try {
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bmp;
	}
	
}
