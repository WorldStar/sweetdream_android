package com.tiseno.sweetdream.bmiresult;


import com.tiseno.sweetdream.SDButtonSprite;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.movie.scene_first;
import com.tiseno.sweetdream.sound.CSound;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.SurfaceHolder.Callback;


public class BmiResultView extends SurfaceView implements Callback {

	private Context m_context = null;
	private DrawThread m_drawThread = null;
	private SurfaceHolder m_holder;
	
	private Bitmap m_img_bg_default = null;
	private Bitmap m_img_bar_top = null;
	private Bitmap m_img_result_page = null;
	private Bitmap m_img_bmi = null;
	
	private SDButtonSprite m_sprt_next_button = null;
	
	private Typeface font1 = null;
	private Paint font_title_paint = null;
	private Paint font_content_paint = null;
	
	private String[] contents = 
		{
			"An appropriate body support during sleep",
			"allow your body's musculoskeletal system to",
			" recover from daily activities.",
			"Your weight distribution on the mattress",
			"should be optimized concerning the impacts of ",
			"the pressure points.",
			"Find your matresss SUPPORT FACTOR INDEX",
			" number to maximize your recovery."
		};
	
	public BmiResultView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub	
	
	}
	
	public BmiResultView(Context context, AttributeSet set)
	{
		super(context, set);
		
		m_context = context;
		
		ShareData.doCalc();
		
		loadResource();
		
		font1 = Typeface.createFromAsset(context.getAssets(), "fonts/CreteRound-Regular.otf");
		Typeface.createFromAsset(context.getAssets(), "fonts/Lobster 1.4.otf");
		
		font_title_paint = new Paint();
		font_title_paint.setDither(true);
		font_title_paint.setAntiAlias(true);
		font_title_paint.setTextAlign(Align.LEFT);
		font_title_paint.setTextSize(40.0f * Utils.m_screenScaleY);
		font_title_paint.setColor(0xff646464);
		font_title_paint.setTypeface(font1);
		
		font_content_paint = new Paint();
		font_content_paint.setDither(true);
		font_content_paint.setAntiAlias(true);
		font_content_paint.setTextAlign(Align.CENTER);
		font_content_paint.setTextSize(24.0f * Utils.m_screenScaleY);
		font_content_paint.setColor(0xff646464);
		font_content_paint.setTypeface(font1);
		
		m_holder = getHolder();
		m_holder.addCallback(this);	
		
		setOnTouchListener(m_touchListener);	
	}
	
	//touch listener
	private OnTouchListener m_touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			switch(event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					if(m_sprt_next_button != null)
						m_sprt_next_button.setTouchDown(event.getX(), event.getY());
					
					break;
				}
				case MotionEvent.ACTION_MOVE:
				{
					if(m_sprt_next_button != null)
						m_sprt_next_button.setTouchMove(event.getX(), event.getY());
					break;
				}
				case MotionEvent.ACTION_UP:
				{
					if(m_sprt_next_button != null)
					{
						if(m_sprt_next_button.contains(event.getX(), event.getY()))
						{
							m_sprt_next_button.setTouchUp(event.getX(), event.getY());
							CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
						}
					}
					break;
				}
			}
			
			return true;
		}
	};

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		m_drawThread = new DrawThread();
		m_drawThread.setDaemon(true);
		m_drawThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
	
		m_drawThread.m_shouldExit = true;
				
		for(;;)
		{
			try {
				m_drawThread.join();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		removeResources();
	}
	
	private void loadResource()
	{
		if(m_img_bg_default == null)
			m_img_bg_default = Utils.loadScreenScaledBitmapXY(m_context, "img/bg_default.png");
		
		if(m_img_bar_top == null)
			m_img_bar_top = Utils.loadScreenScaledBitmapXY(m_context, "img/bar_top.png");
		
		if(m_img_bmi == null)
			m_img_bmi = Utils.loadScreenScaledBitmapY(m_context, ShareData.BMI_IMAGE_PATH);

		if(m_sprt_next_button == null)
		{
			m_sprt_next_button = SDButtonSprite.createFromAsset(m_context, "img/btn_next_large.png");
			m_sprt_next_button.setNavigateHandler(buttonNextNavigateHandler);
		}
		
		if(m_img_result_page == null)
			m_img_result_page = Utils.loadScreenScaledBitmapY(m_context, "img/resultpage.png");			
	}
	
	private void removeResources()
	{
		if(m_img_bg_default != null)
		{
			m_img_bg_default.recycle();
			m_img_bg_default = null;
		}
		
		if(m_img_bar_top != null)
		{
			m_img_bar_top.recycle();
			m_img_bar_top = null;
		}
		
		if(m_img_bmi != null)
		{
			m_img_bmi.recycle();
			m_img_bmi = null;
		}

		if(m_sprt_next_button != null)
		{
			m_sprt_next_button.destroy();
			m_sprt_next_button = null;
		}
		
		if(m_img_result_page != null)
		{
			m_img_result_page.recycle();
			m_img_result_page = null;
		}
	}
	
	private Handler buttonNextNavigateHandler = new Handler(new Handler.Callback() {			
		@Override
		public boolean handleMessage(Message msg) {
			
			m_drawThread.m_shouldExit = true;
			
			ShareData.Activity_Navigator = true;
			
			((BmiResultActivity)m_context).finish();
			
			// TODO Auto-generated method stub
			Intent intent = new Intent(m_context, scene_first.class);
			((BmiResultActivity)m_context).startActivity(intent);
			
			return true;
		}
	});	
	
	
	public class DrawThread extends Thread
	{
		public boolean m_shouldExit = false;
		private Canvas m_canvas = null;
		private Paint m_paint = new Paint();
		
		public void run(){
			m_paint.setAntiAlias(true);
			m_paint.setDither(true);
			
			while(!m_shouldExit)
			{
				if(m_shouldExit)
					break;
				
//				long strtTime = System.currentTimeMillis();
				
				synchronized (m_holder) {					
					try{
						m_canvas = m_holder.lockCanvas(null);
						drawEngine();						
					}catch (Exception e) {
						// TODO: handle exception						
					}finally
					{
						try
						{
							m_holder.unlockCanvasAndPost(m_canvas);
						}catch(Exception e)
						{
							;
						}
					}
				}
				
				try {
					sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
			}
		}
		
		public void drawEngine()
		{			
			loadResource();
			drawResource();			
		}		
		
		private void drawResource()
		{
			//draw bg
			if(m_img_bg_default != null)
				m_canvas.drawBitmap(m_img_bg_default, 0, 0, m_paint);
			
			if(m_img_result_page != null)
				m_canvas.drawBitmap(m_img_result_page, (Utils.m_screenWidth - m_img_result_page.getWidth())/2.0f, 20.0f * Utils.m_screenScaleY, m_paint);
			
			float _x = (Utils.m_screenWidth - m_img_bmi.getWidth())/2.0f;
			float _y = 110.0f * Utils.m_screenScaleY;
			if(m_img_bmi != null)
				m_canvas.drawBitmap(m_img_bmi, _x, _y, m_paint);
			
			// draw bmi text
			_x = _x +  462 * Utils.m_screenScaleY;
			_y = _y + 80 * Utils.m_screenScaleY;
			float val = ((int)(ShareData.BMI_VALUE * 10)) / 10.0f;
			m_canvas.drawText(String.valueOf(val), _x, _y, font_title_paint);
				
			// draw content
			_x = Utils.m_screenWidth / 2.0f;
			_y = 370 * Utils.m_screenScaleY;
			
			for(int i = 0; i < contents.length; i++)
			{
				String s = contents[i];
				m_canvas.drawText(s, _x, _y, font_content_paint);
				if((i % 3) == 2)
					_y += 60.0f * Utils.m_screenScaleY;
				else
					_y += 30.0f * Utils.m_screenScaleY;
			}
			
			// draw top bar
			m_canvas.drawBitmap(m_img_bar_top, 0, 0, m_paint);
			
			// draw next button
			if(m_sprt_next_button.getBounds() == null)
			{
				float x = (Utils.m_screenWidth - m_sprt_next_button.getWidth()) / 2.0f;
				float y = (Utils.m_screenHeight + m_img_result_page.getHeight() - m_sprt_next_button.getHeight()) / 2.0f + 20.0f * Utils.m_screenScaleY;
				RectF rect = new RectF(x, y, x + m_sprt_next_button.getWidth(), y + m_sprt_next_button.getHeight());
				m_sprt_next_button.setBounds(rect);
				m_sprt_next_button.setNavigateHandler(buttonNextNavigateHandler);
			}

			m_sprt_next_button.drawSprite(m_canvas, m_paint);
		}
	
		
	}

}
