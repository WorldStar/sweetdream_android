package com.tiseno.sweetdream;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;

public class SDChooseGenderBoySprite {
	
	private Context m_context = null;
	
	public static float body_bottom_y = 30.0f;
		
	private Bitmap m_img_mail_head = null;
	private Bitmap m_img_normal_body = null;
	private Bitmap m_img_male_eye = null;
	private Bitmap m_img_male_eye_close = null;
	private Bitmap m_img_male_eye_left = null;
	private Bitmap m_img_male_eye_right = null;
	private Bitmap m_img_male_eyebrown_left= null;
	private Bitmap m_img_male_eyebrown_right = null;
	private Bitmap m_img_male_mouth = null;
	private Bitmap m_img_male_mouth_2 = null;
	private Bitmap m_img_male_mouth_3 = null;
	private Bitmap m_img_male_mouth_4 = null;
	
	
    private final int face_original = -1;
	private final int face_anim_1 = 0;
	private final int face_anim_2 = 1;
	private final int face_anim_3 = 2;
	private final int face_anim_4 = 3;
	
	private int anim_status = face_original;
	
	private float rotate_interval = 2000;
	private float face_interval = 3000;
	
	private long rotate_curr_time = 0;
	private long face_curr_time = 0;
	private boolean rotated = false; 
	private boolean faced = false;
	
	private Random rand = new Random(System.currentTimeMillis());
		
	float head_x = 0;
	float head_y = 0;

	private boolean isStartedMoveRight = false;

	private boolean isStartedMoveLeft = false;

	private long move_time= 0;	
	
	private boolean isShown = true;

	private boolean isTouchDown = false;
	private long zoom_time = 0;
	private float zoom_val = 1;

	private Handler mChangeGenderHandler = null;
	
	public SDChooseGenderBoySprite(Context context)
	{
		m_context = context;	
		
		loadAllResources();
	}
	
	private void loadAllResources()
	{
		isShown = true;
		isStartedMoveRight = false;
		isStartedMoveLeft = false;
		move_time = 0;
		isTouchDown = false;
		zoom_time = 0;
		zoom_val = 1;
		
		mChangeGenderHandler = null;
		
		m_img_mail_head = Utils.loadScreenScaledBitmapY(m_context, "img/male_head.png");
		
		if(ShareData.Selected_Weight < 50 && ShareData.Selected_Weight > 0)
			m_img_normal_body = Utils.loadScreenScaledBitmapY(m_context, "img/male_skinny_body.png");
		else if((ShareData.Selected_Weight >= 50 && ShareData.Selected_Weight < 80) || (ShareData.Selected_Weight == -1) || (ShareData.Selected_Weight == 0))
			m_img_normal_body = Utils.loadScreenScaledBitmapY(m_context, "img/male_normal_body.png");
		else if(ShareData.Selected_Weight >= 80)
			m_img_normal_body = Utils.loadScreenScaledBitmapY(m_context, "img/male_fat_body.png");
		
		m_img_normal_body = Utils.loadScreenScaledBitmapY(m_context, "img/male_normal_body.png");
		m_img_male_eye = Utils.loadScreenScaledBitmapY(m_context, "img/male_eye.png");
		m_img_male_eye_close = Utils.loadScreenScaledBitmapY(m_context, "img/male_eye_close.png");
		m_img_male_eye_left = Utils.loadScreenScaledBitmapY(m_context, "img/male_eye_left.png");
		m_img_male_eye_right = Utils.loadScreenScaledBitmapY(m_context, "img/male_eye_right.png");
		m_img_male_eyebrown_left = Utils.loadScreenScaledBitmapY(m_context, "img/male_eyebrown_left.png");
		m_img_male_eyebrown_right = Utils.loadScreenScaledBitmapY(m_context, "img/male_eyebrown_right.png");
		m_img_male_mouth = Utils.loadScreenScaledBitmapY(m_context, "img/male_mouth.png");
		m_img_male_mouth_2 = Utils.loadScreenScaledBitmapY(m_context, "img/male_mouth_2.png");
		m_img_male_mouth_3 = Utils.loadScreenScaledBitmapY(m_context, "img/male_mouth_3.png");
		m_img_male_mouth_4 = Utils.loadScreenScaledBitmapY(m_context, "img/male_mouth_4.png");		
	}
	
	public void drawCanvas(Canvas canvas, Paint paint)
	{		
		float scr_w = Utils.m_screenWidth;
		float scr_h = Utils.m_screenHeight;
		
		float d_x = 0;
		if(isStartedMoveLeft == true && isStartedMoveRight == false)
		{
			if(move_time == 0)
				move_time = System.currentTimeMillis();
			
			float p =  (System.currentTimeMillis() - move_time) / 500f;
			if(p > 1)
			{
				p = 1;
				isShown = false;
				isStartedMoveLeft = false;
				isStartedMoveRight = false;
				move_time = 0;
			}
			
			d_x = p * scr_w;
		}else if(isStartedMoveLeft == false && isStartedMoveRight == true)		
		{
			if(move_time == 0)
				move_time = System.currentTimeMillis();
			
			float p =  (System.currentTimeMillis() - move_time) / 500f;
			if(p > 1)
			{
				p = 1;
				isShown = true;
				isStartedMoveLeft = false;
				isStartedMoveRight = false;
				move_time = 0;
			}
			d_x = p * scr_w;
			d_x = scr_w - d_x;
		}else if(isStartedMoveLeft == false && isStartedMoveRight == false)
		{
			if(isShown == true)
			{
				d_x = 0;
			}else
			{
				d_x = -scr_w;
			}
		}
		
		if(isTouchDown)
		{
			if(zoom_time == 0)
				zoom_time = System.currentTimeMillis();
			zoom_val = 1.025f;
			if((System.currentTimeMillis() - zoom_time) > 200)
			{
				isTouchDown = false;
				zoom_time = 0;
				zoom_val = 1;
				
				if(mChangeGenderHandler != null)
					mChangeGenderHandler.sendEmptyMessageDelayed(0, 150);
			}
		}
		
		canvas.save();
		canvas.scale(zoom_val, zoom_val, Utils.m_screenWidth / 2.0f, Utils.m_screenHeight / 2.0f);
		
		//draw body
		float x = (scr_w - m_img_normal_body.getWidth()) / 2.0f - d_x;
		float y = scr_h - ((body_bottom_y * Utils.m_screenScaleY) + m_img_normal_body.getHeight());
		canvas.drawBitmap(m_img_normal_body, x, y, paint);
		
		//draw head
		head_x = (scr_w - m_img_mail_head.getWidth()) / 2.0f - d_x;
		head_y = y - m_img_mail_head.getHeight() + 20.0f * Utils.m_screenScaleY;
	
		if(rotate_curr_time == 0)
			rotate_curr_time = System.currentTimeMillis();
		
		if(face_curr_time == 0)
			face_curr_time = System.currentTimeMillis();
		
		long rotate_diff_time = System.currentTimeMillis() - rotate_curr_time;
		
		if(rotate_diff_time > rotate_interval)
		{
			rotate_curr_time = System.currentTimeMillis();
			rotated = !rotated;
		}
		
		long face_diff_time = System.currentTimeMillis() - face_curr_time;
		
		if(face_diff_time >= face_interval)
		{
			face_curr_time = System.currentTimeMillis();
			faced = !faced;
			if(faced)
			{				
				anim_status = Math.abs(rand.nextInt() % 4);
			}
		}
		
		if(rotated)
		{
			rotate_interval = 2000;
			float angle = ((System.currentTimeMillis() - rotate_curr_time) / 300.0f) * 18.0f;
			if(angle > 15.0f)
				angle = 15.0f;
			
			canvas.save();
			canvas.rotate(angle, head_x + m_img_mail_head.getWidth() / 2.0f, head_y + m_img_mail_head.getHeight());
		}else
		{
			rotate_interval = 8000;
			float angle = 15.0f - ((System.currentTimeMillis() - rotate_curr_time) / 300.0f) * 15.0f;
			if(angle < 0)
				angle = 0;
			
			canvas.save();
			canvas.rotate(angle, head_x + m_img_mail_head.getWidth() / 2.0f, head_y + m_img_mail_head.getHeight());
		}
		
		if(faced)
		{
			face_interval = 600;
		}else
		{
			face_interval = 4000;
			anim_status = face_original;
		}		
		
		canvas.drawBitmap(m_img_mail_head, head_x, head_y, paint);
		
		switch(anim_status)
		{
			case face_original:
			{
				//draw left eyebrow	
				float brown_x = head_x + m_img_mail_head.getWidth() * 0.3f - (m_img_male_eyebrown_left.getWidth() / 2.0f);
				float brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_left.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eyebrown_left, brown_x, brown_y, paint);
				
				//draw right eyebrow	
				brown_x = head_x + m_img_mail_head.getWidth() * 0.7f - (m_img_male_eyebrown_right.getWidth() / 2.0f);
				brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_right.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eyebrown_right, brown_x, brown_y, paint);
				
				//draw mouth
				float mouth_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_mouth.getWidth() / 2.0f);
				float mouth_y = head_y + m_img_mail_head.getHeight() * 0.85f - (m_img_male_mouth.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_mouth, mouth_x, mouth_y, paint);
				
				//draw eye
				float eye_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_eye.getWidth() / 2.0f);
				float eye_y = head_y + m_img_mail_head.getHeight() * 0.7f - (m_img_male_eye.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eye, eye_x, eye_y, paint);
				
				break;
			}
			case face_anim_1:
			{
				//draw left eyebrow	
				float brown_x = head_x + m_img_mail_head.getWidth() * 0.3f - (m_img_male_eyebrown_left.getWidth() / 2.0f);
				float brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_left.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eyebrown_left, brown_x, brown_y, paint);
				
				//draw right eyebrow	
				brown_x = head_x + m_img_mail_head.getWidth() * 0.7f - (m_img_male_eyebrown_right.getWidth() / 2.0f);
				brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_right.getHeight() / 2.0f);
				
				canvas.save();
				canvas.rotate(10, brown_x + m_img_male_eyebrown_right.getWidth(), brown_y + m_img_male_eyebrown_right.getHeight());
				
				canvas.drawBitmap(m_img_male_eyebrown_right, brown_x, brown_y, paint);
				
				canvas.restore();
				
				//draw mouth
				float mouth_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_mouth_2.getWidth() / 2.0f);
				float mouth_y = head_y + m_img_mail_head.getHeight() * 0.85f - (m_img_male_mouth_2.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_mouth_2, mouth_x, mouth_y, paint);
				
				//draw eye
				float eye_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_eye_right.getWidth() / 2.0f);
				float eye_y = head_y + m_img_mail_head.getHeight() * 0.7f - (m_img_male_eye_right.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eye_right, eye_x, eye_y, paint);
				
				break;
			}
			case face_anim_2:
			{
				//draw left eyebrow	
				float brown_x = head_x + m_img_mail_head.getWidth() * 0.3f - (m_img_male_eyebrown_left.getWidth() / 2.0f);
				float brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_left.getHeight() / 2.0f) - 6 * Utils.m_screenScaleY;

				canvas.drawBitmap(m_img_male_eyebrown_left, brown_x, brown_y, paint);
				
				//draw right eyebrow	
				brown_x = head_x + m_img_mail_head.getWidth() * 0.7f - (m_img_male_eyebrown_right.getWidth() / 2.0f);
				brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_right.getHeight() / 2.0f) - 6 * Utils.m_screenScaleY;
				canvas.drawBitmap(m_img_male_eyebrown_right, brown_x, brown_y, paint);
				
				//draw mouth
				float mouth_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_mouth_3.getWidth() / 2.0f);
				float mouth_y = head_y + m_img_mail_head.getHeight() * 0.85f - (m_img_male_mouth_3.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_mouth_3, mouth_x, mouth_y, paint);
				
				//draw eye
				float eye_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_eye_left.getWidth() / 2.0f);
				float eye_y = head_y + m_img_mail_head.getHeight() * 0.7f - (m_img_male_eye_left.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eye_left, eye_x, eye_y, paint);
				break;
			}
			case face_anim_3:
			{
				//draw left eyebrow	
				float brown_x = head_x + m_img_mail_head.getWidth() * 0.3f - (m_img_male_eyebrown_left.getWidth() / 2.0f);
				float brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_left.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eyebrown_left, brown_x, brown_y, paint);
				
				//draw right eyebrow	
				brown_x = head_x + m_img_mail_head.getWidth() * 0.7f - (m_img_male_eyebrown_right.getWidth() / 2.0f);
				brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_right.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eyebrown_right, brown_x, brown_y, paint);
				
				//draw mouth
				float mouth_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_mouth_4.getWidth() / 2.0f);
				float mouth_y = head_y + m_img_mail_head.getHeight() * 0.85f - (m_img_male_mouth_4.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_mouth_4, mouth_x, mouth_y, paint);
				
				//draw eye
				float eye_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_eye_close.getWidth() / 2.0f);
				float eye_y = head_y + m_img_mail_head.getHeight() * 0.7f - (m_img_male_eye_close.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eye_close, eye_x, eye_y, paint);
				break;
			}
			case face_anim_4:
			{
				//draw left eyebrow	
				float brown_x = head_x + m_img_mail_head.getWidth() * 0.3f - (m_img_male_eyebrown_left.getWidth() / 2.0f);
				float brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_left.getHeight() / 2.0f)  - 6 * Utils.m_screenScaleY;
				canvas.drawBitmap(m_img_male_eyebrown_left, brown_x, brown_y, paint);
				
				//draw right eyebrow	
				brown_x = head_x + m_img_mail_head.getWidth() * 0.7f - (m_img_male_eyebrown_right.getWidth() / 2.0f);
				brown_y = head_y + m_img_mail_head.getHeight() * 0.58f - (m_img_male_eyebrown_right.getHeight() / 2.0f)  - 6 * Utils.m_screenScaleY;
				canvas.drawBitmap(m_img_male_eyebrown_right, brown_x, brown_y, paint);
				
				//draw mouth
				float mouth_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_mouth_4.getWidth() / 2.0f);
				float mouth_y = head_y + m_img_mail_head.getHeight() * 0.85f - (m_img_male_mouth_4.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_mouth_4, mouth_x, mouth_y, paint);
				
				//draw eye
				float eye_x = head_x + m_img_mail_head.getWidth() * 0.5f - (m_img_male_eye.getWidth() / 2.0f);
				float eye_y = head_y + m_img_mail_head.getHeight() * 0.7f - (m_img_male_eye.getHeight() / 2.0f);
				canvas.drawBitmap(m_img_male_eye, eye_x, eye_y, paint);
				break;
			}
		}
		
		canvas.restore();
		canvas.restore();
	}
	
	public void removeResource()
	{
		if(m_img_mail_head != null)
		{
			m_img_mail_head.recycle();
			m_img_mail_head = null;
		}
		
		if(m_img_normal_body != null)
		{
			m_img_normal_body.recycle();
			m_img_normal_body = null;
		}
		
		if(m_img_male_eye != null)
		{
			m_img_male_eye.recycle();
			m_img_male_eye = null;
		}
		
		if(m_img_male_eye_close != null)
		{
			m_img_male_eye_close.recycle();
			m_img_male_eye_close = null;
		}
		
		if(m_img_male_eye_left != null)
		{
			m_img_male_eye_left.recycle();
			m_img_male_eye_left = null;
		}
		
		if(m_img_male_eye_right != null)
		{
			m_img_male_eye_right.recycle();
			m_img_male_eye_right = null;
		}
	}

	public boolean isMovingAnimation()
	{
		return (isStartedMoveLeft || isStartedMoveRight);				
	}
	
	public void startMoveRight() {
		// TODO Auto-generated method stub
		if(isStartedMoveLeft || isStartedMoveRight)
			return;
		
		move_time = 0;
		isStartedMoveLeft = false;
		isStartedMoveRight = true;
	}	

	public void startMoveLeft() {
		// TODO Auto-generated method stub
		if(isStartedMoveLeft || isStartedMoveRight)
			return;
		
		move_time = 0;
		isStartedMoveRight = false;
		isStartedMoveLeft = true;
	}

	public boolean contains(float x, float y) {
		// TODO Auto-generated method stub
		if(isStartedMoveLeft || isStartedMoveRight || !isShown)
			return false;
		
		boolean contains = false;
		
		//draw body
		float x_body = (Utils.m_screenWidth - m_img_normal_body.getWidth()) / 2.0f;
		float y_body = Utils.m_screenHeight - ((body_bottom_y * Utils.m_screenScaleY) + m_img_normal_body.getHeight());
		y_body = y_body - m_img_mail_head.getHeight() + 20.0f * Utils.m_screenScaleY;
		
		RectF rect = new RectF(x_body, y_body, x_body + m_img_normal_body.getWidth(), y_body + m_img_mail_head.getHeight() + m_img_normal_body.getHeight());
		if(rect.contains(x, y))
		{
			contains = true;
		}
		
		return contains;
	}

	public void setTouchDown(float x, float y) {
		// TODO Auto-generated method stub
		isTouchDown  = true;
		ShareData.Selected_Gender = ShareData.Gender_Mail;
	}

	public void addChooseGenderHandler(Handler changeGenderHandler) {
		// TODO Auto-generated method stub
		mChangeGenderHandler  = changeGenderHandler;
		
	}	
}
