package com.tiseno.sweetdream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;

public class SDButtonSprite {

	private Bitmap m_bmp = null;
	private RectF m_bound = null;
	
	private Paint m_filterPaint = null;
	private Paint m_disableFilterPaint = null;
	
	private final int TOUCH_NULL = 0;
	private final int TOUCH_DOWN = 1;
	private final int TOUCH_MOVE = 2;
	private final int TOUCH_UP = 3;
	
	private int m_touchStatus = TOUCH_NULL;
	private Handler navigateHandler = null;
	private boolean m_Enabled = true;

	
	private SDButtonSprite(Bitmap bmp)
	{
		m_bmp = bmp;
		m_bound = null;
		m_touchStatus = TOUCH_NULL;
		m_Enabled = true;
		
		m_filterPaint = new Paint();
		m_filterPaint.setDither(true);
		m_filterPaint.setAntiAlias(true);
		m_filterPaint.setColorFilter(new LightingColorFilter(0x666666, 0x000000));
		
		m_disableFilterPaint  = new Paint();
		m_disableFilterPaint = new Paint();
		m_disableFilterPaint.setDither(true);
		m_disableFilterPaint.setAntiAlias(true);
		m_disableFilterPaint.setAlpha(100);
		
	}
	
	public static SDButtonSprite createFromAsset(Context context, String img_path)
	{
		Bitmap bmp = Utils.loadScreenScaledBitmapY(context, img_path);
		
		SDButtonSprite sprite = new SDButtonSprite(bmp);
		
		return sprite;
	}
	
	
	public RectF getBounds()
	{
		return m_bound;
	}
	
	public void setNavigateHandler(Handler h)
	{
		navigateHandler = h;
	}
	
	public void setBounds(RectF rect)
	{
		m_bound = rect;
	}
	
	public void drawSprite(Canvas canvas, Paint paint)
	{
		if(m_bmp != null && m_bound != null && !m_bound.isEmpty())
		{
			if(!m_Enabled)
			{
				canvas.drawBitmap(m_bmp, m_bound.left, m_bound.top, m_disableFilterPaint);
				return;
			}
			
			switch(m_touchStatus)
			{			
				case TOUCH_NULL:
				{
					canvas.drawBitmap(m_bmp, m_bound.left, m_bound.top, paint);
					break;
				}
				case TOUCH_DOWN:
				{
					canvas.drawBitmap(m_bmp, m_bound.left, m_bound.top, m_filterPaint);
					break;
				}
				case TOUCH_MOVE:
				{
					canvas.drawBitmap(m_bmp, m_bound.left, m_bound.top, m_filterPaint);
					break;
				}
				case TOUCH_UP:
				{
					canvas.drawBitmap(m_bmp, m_bound.left, m_bound.top, paint);
					break;
				}
			}
		}
	}
	
	public boolean contains(float x, float y)
	{
		boolean isContained = false;
		if(m_bound == null || m_bound.isEmpty())
			return isContained;
		
		isContained = m_bound.contains(x, y);
		return isContained;
	}
	
	public float getWidth()
	{
		float w = 0;
		if(m_bmp != null)
		{
			w = m_bmp.getWidth();
		}
		
		return w;
	}
	
	public float getHeight()
	{
		float h = 0;
		if(m_bmp != null)
		{
			h = m_bmp.getHeight();
		}
		
		return h;
	}
	
	public void destroy()
	{
		if(m_bmp != null)
		{
			m_bmp.recycle();
			m_bmp = null;
		}
		
		m_bound = null;
	}

	public void setTouchDown(float x, float y) {
		// TODO Auto-generated method stub
		if(this.contains(x, y))
		{
			m_touchStatus = TOUCH_DOWN;	
		}
			
	}

	public void setTouchMove(float x, float y) {
		// TODO Auto-generated method stub
		
		if(m_touchStatus != TOUCH_NULL)
		{
			boolean contain = this.contains(x, y);
			if(!contain)
			{
				m_touchStatus = TOUCH_NULL;
				return;
			}
			
			m_touchStatus = TOUCH_MOVE;
		}
	}

	public void setTouchUp(float x, float y) {
		// TODO Auto-generated method stub
		if(m_touchStatus == TOUCH_DOWN || m_touchStatus == TOUCH_MOVE)
		{
			m_touchStatus = TOUCH_NULL;
			if(navigateHandler != null)
				navigateHandler.sendEmptyMessageDelayed(0, 250);
		}		
	}

	public void setEnable(boolean b) {
		// TODO Auto-generated method stub
		m_Enabled  = b;
	}
}
