package com.tiseno.sweetdream;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class SDClock {
	private Context m_context = null;
	
	private Bitmap img_clock = null;
	private Bitmap img_hr_hand = null;
	private Bitmap img_min_hand = null;
	private Bitmap img_sec_hand = null;
	
	private boolean isStarted = false;
	
	public SDClock(Context context)
	{
		m_context = context;
		isStarted = false;
		loadResource();
	}
		
	private void loadResource()
	{
		img_clock = Utils.loadScreenScaledBitmapY(m_context, "img/clock.png");
		img_hr_hand = Utils.loadScreenScaledBitmapY(m_context, "img/hr_hand.png");
		img_min_hand = Utils.loadScreenScaledBitmapY(m_context, "img/min_hand.png");
		img_sec_hand = Utils.loadScreenScaledBitmapY(m_context, "img/sec_hand.png");
	}
	
	public void drawCanvas(Canvas canvas, Paint paint)
	{
		//draw clock board
		float x = 20 * Utils.m_screenScaleY;
		float y = 100 * Utils.m_screenScaleY;
		canvas.drawBitmap(img_clock, x, y, paint);
		
		Calendar cal = new GregorianCalendar();
		int h = cal.get(Calendar.HOUR_OF_DAY) % 12;
		int m = cal.get(Calendar.MINUTE);
		int s = cal.get(Calendar.SECOND);
		
		float pivot_x = x + (img_clock.getWidth() / 2.0f);
		float pivot_y = y + (img_clock.getHeight() / 2.0f); 
		
		canvas.save();
		canvas.rotate((h / 12.0f * 360) + ((m / 12) / 60.0f * 360), pivot_x, pivot_y);
//		//draw hour
		float x_hour = pivot_x - img_hr_hand.getWidth() / 2.0f;
		float y_hour = pivot_y - img_hr_hand.getHeight() + 10.0f * Utils.m_screenScaleY;		
		canvas.drawBitmap(img_hr_hand, x_hour, y_hour,paint);
		
		canvas.restore();
		
		canvas.save();
		canvas.rotate(m / 60.0f * 360, pivot_x, pivot_y);
		
		//draw min
		float x_min = pivot_x - img_min_hand.getWidth() / 2.0f;
		float y_min = pivot_y - img_min_hand.getHeight() + 10.0f * Utils.m_screenScaleY;		
		canvas.drawBitmap(img_min_hand, x_min, y_min,paint);
		
		canvas.restore();
		
		canvas.save();
		canvas.rotate(s / 60.0f * 360, pivot_x, pivot_y);
		
		//draw sec
		float x_sec = pivot_x - img_sec_hand.getWidth() / 2.0f;
		float y_sec = pivot_y - img_sec_hand.getHeight() + 10.0f * Utils.m_screenScaleY;		
		canvas.drawBitmap(img_sec_hand, x_sec, y_sec, paint);
		
		canvas.restore();
	}
	
	public void startClock()
	{
		if(isStarted)
			return;
		
		isStarted = true;
	}
	
	public void removeResource()
	{
		if(img_clock != null)
		{
			img_clock.recycle();
			img_clock = null;
		}
		
		if(img_hr_hand != null)
		{
			img_hr_hand.recycle();
			img_hr_hand = null;
		}
		
		if(img_min_hand != null)
		{
			img_min_hand.recycle();
			img_min_hand = null;
		}
		
		if(img_sec_hand != null)
		{
			img_sec_hand.recycle();
			img_sec_hand = null;
		}
	}
	
}
