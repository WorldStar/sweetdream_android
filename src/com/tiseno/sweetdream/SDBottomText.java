package com.tiseno.sweetdream;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;

public class SDBottomText {

	private String m_text = null;
	private Typeface font1 = null;
	private Paint paint = null;
	private boolean isStarted = false;
	private long current_time = 0;
	
	public SDBottomText(Context context, String text)
	{
		m_text = text;
		
		font1 = Typeface.createFromAsset(context.getAssets(), "fonts/CreteRound-Regular.otf");
		Typeface.createFromAsset(context.getAssets(), "fonts/Lobster 1.4.otf");
		
		paint = new Paint();
		paint.setDither(true);
		paint.setAntiAlias(true);
		paint.setTextAlign(Align.CENTER);
		paint.setTextSize(50.0f * Utils.m_screenScaleY);
		paint.setTypeface(font1);
		
		isStarted = true;
	}
	
	public void drawText(Canvas canvas, int level)
	{
		if(m_text != null)
		{	
			float dist = (60.0f * level) * Utils.m_screenScaleY;
			float target_x = Utils.m_screenWidth / 2.0f;
			float target_y = Utils.m_screenHeight - dist;
			
			if(isStarted)
			{
				if(current_time == 0)
					current_time = System.currentTimeMillis();
				
				long time = System.currentTimeMillis();
				float d = (float)((time - current_time) / 500.0f) *  dist;
				
				float y = 0.0f;
				if(d > dist)
				{
					isStarted = false;
					y = target_y;
				}else
				{
					y = Utils.m_screenHeight - d;
				}
				
				paint.setColor(Color.BLACK);
				canvas.drawText(m_text, target_x, y, paint);
				
				paint.setColor(Color.WHITE);
				canvas.drawText(m_text, target_x + 3 * Utils.m_screenScaleX, y - 3 * Utils.m_screenScaleY, paint);
			}else
			{
				paint.setColor(Color.BLACK);
				canvas.drawText(m_text, target_x, target_y, paint);
				
				paint.setColor(Color.WHITE);
				canvas.drawText(m_text, target_x + 3 * Utils.m_screenScaleX, target_y - 3 * Utils.m_screenScaleY, paint);
			}

		}
	}
}
