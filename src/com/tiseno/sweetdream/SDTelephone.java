package com.tiseno.sweetdream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;

public class SDTelephone {
	
	private Bitmap telephone = null;
	private Context m_context = null;
	private Handler handler = null;
	
	private float m_left = 0;
	private float m_top = 0;
	
	public SDTelephone(Context context)
	{
		m_context = context;
		loadResource();
	}
	
	private void loadResource()
	{
		telephone = Utils.loadScreenScaledBitmapY(m_context, "img/telephone.png");
	}
	
	public void DrawCanvas(Canvas canvas, Paint paint)
	{
		m_left = Utils.m_screenWidth * 0.875f - telephone.getWidth() / 2.0f;
		m_top = Utils.m_screenHeight * 0.38f - telephone.getHeight() / 2.0f;
		canvas.drawBitmap(telephone, m_left, m_top, paint);
	}	
	
	public void removeResource()
	{
		if(telephone != null)
		{
			telephone.recycle();
			telephone = null;
		}
	}

	public void addClickHandler(Handler callPhoneHandler) {
		// TODO Auto-generated method stub
		handler = callPhoneHandler;		
	}

	public boolean contains(float x, float y) {
		// TODO Auto-generated method stub
		boolean contained = false;
		RectF rect = new RectF(m_left, m_top, m_left + telephone.getWidth(), m_top + telephone.getHeight());
		contained = rect.contains(x,y);
		return contained;
	}

	public void setTouchDown(float x, float y) {
		// TODO Auto-generated method stub
		if(handler != null)
			handler.sendEmptyMessageDelayed(0, 100);
	}
}
