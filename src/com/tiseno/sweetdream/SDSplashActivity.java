package com.tiseno.sweetdream;

import com.tiseno.sweetdream.sound.CSound;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

public class SDSplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		Utils.caculateScales(this);
		
		SDSplashView view = new SDSplashView(this);		
		setContentView(view);
		overridePendingTransition(0, R.anim.exitanim);
		
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
//				overridePendingTransition(0, R.anim.zoom_exit);
				
				ShareData.Activity_Navigator = true;
				
				finish();				
				Intent intent = new Intent(SDSplashActivity.this, SDStartActivity.class);
				startActivity(intent);				
				
			}
		}, 1500);
		
		CSound.initSoundController(this);
		CSound.sharedSoundController().playMusic("bg_music.mp3");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub		
		super.onDestroy();
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
		{
			return true;
		}
		
		if (event.getKeyCode() == KeyEvent.KEYCODE_HOME)
		{
			CSound.sharedSoundController().stopMusic();
			return true;
		}
		
		return false;
	}
}
