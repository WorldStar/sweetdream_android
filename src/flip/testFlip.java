package flip;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import com.aphidmobile.flip.FlipViewController;
import com.quietlycoding.android.picker.NumberPicker;
import com.tiseno.sweetdream.R;
import com.tiseno.sweetdream.ShareData;
import com.tiseno.sweetdream.Utils;
import com.tiseno.sweetdream.measure.MeasureWeightActivity;
import com.tiseno.sweetdream.sound.CSound;
import com.tiseno.sweetdream.supportfactor.SupportFactor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

public class testFlip extends Activity {
	  protected FlipViewController flipView;
	private LinearLayout linear = null;
	private View popupView = null;
	private PopupWindow popup = null;
	
	public static final String FACEBOOK_PERMISSION = "publish_stream";
	public static final String FACEBOOK_MSG = "Message from Application";
	
    private Session.StatusCallback statusCallback = 
            new Session.StatusCallback() {
            @Override
            public void call(Session session, 
                    SessionState state, Exception exception) {
                onSessionStateChange(session, state, exception);
            }
        };
	private WebDialog feedDialog;
	private boolean isClicked = true;
	
	private Typeface font = null;
	private boolean popuped = false;;

	  /**
	   * Called when the activity is first created.
	   */
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	    setContentView(R.layout.documentation);
	    
	    overridePendingTransition(R.anim.enterup, R.anim.exitup);

	    font = Typeface.createFromAsset(getAssets(), "fonts/CreteRound-Regular.otf");
	    
	    ((TextView)findViewById(R.id.txt_unit)).setTypeface(font);
	    
	    flipView = (FlipViewController) findViewById(R.id.flipView);

	    flipView.setOnViewFlipListener(new FlipViewController.ViewFlipListener() {
			
			@Override
			public void onViewFlipped(View view, int position) {
				// TODO Auto-generated method stub
				if(position == 0)
				{
					((TextView)testFlip.this.findViewById(R.id.txt_index)).setText("Index");
				}
				else
				{					
					String faq = "FAQ " + String.valueOf(position) + "/13";
					((TextView)testFlip.this.findViewById(R.id.txt_index)).setText(faq);
				}
				
				
			}
		});
	    flipView.setAdapter(new MyBaseAdapter(this, flipView));
 
	    flipView.refreshAllPages();
	    
		//facebook connector initialization************************
		 Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
		 Session session = Session.getActiveSession();
		 if (session == null) { 
			 if (savedInstanceState != null) {
				 session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
			 }
			 if (session == null) {
				 session = new Session(this);
			 }
			 Session.setActiveSession(session);
			 if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				 session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			 }
		 }

		 try {
			 PackageInfo info = getPackageManager().getPackageInfo(
					 getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
			 for (Signature signature : info.signatures) 
			 {
				 MessageDigest md = MessageDigest.getInstance("SHA");
				 md.update(signature.toByteArray());
				 Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			 }

		 } catch (NameNotFoundException ex) {
		 } catch (NoSuchAlgorithmException ex2) {
		 }
	  }
	  
		@Override
		protected void onStart() {
			super.onStart();			
			Session.getActiveSession().addCallback(statusCallback);
		}
		
		@Override
		public void onStop() {
			super.onStop();

			Session.getActiveSession().removeCallback(statusCallback);
		}

	  @Override
	  protected void onResume() {
	    super.onResume();
	    flipView.onResume();
	    
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.play();
		}else
		{
			ShareData.Activity_Navigator = false;
		}
	  }

	  @Override
	  protected void onPause() {
	    super.onPause();
	    flipView.onPause();
	    
		if(!ShareData.Activity_Navigator)
		{
			if(CSound.sharedSoundController() != null && CSound.sharedSoundController().musicTrack != null)
				CSound.sharedSoundController().musicTrack.pause();
		}
	  }
	  
	  
	  public void onGotoWeb(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
		  
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Uri uri = Uri.parse("http://sweetdream.com.my");
			intent.setData(uri);
			testFlip.this.startActivity(intent);
	  }
	  
	  public void onPrevPage(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
		  
		  flipView.setFlipByTouchEnabled(false);
		  int id = flipView.getSelectedItemPosition();
		  id -= 1;
		  if(id < 0) id = 0;
		  flipView.setSelection(id);
		  
		  if(id > 0)
		  {
			  String faq = "FAQ " + String.valueOf(id) + "/13";
			((TextView)testFlip.this.findViewById(R.id.txt_index)).setText(faq);
		  }else
		  {
			  ((TextView)testFlip.this.findViewById(R.id.txt_index)).setText("Index");
		  }
		  flipView.setFlipByTouchEnabled(true);
	  }
	  
	  public void onNextPage(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
		  
		  flipView.setFlipByTouchEnabled(false);
		  int id = flipView.getSelectedItemPosition();
		  id += 1;
		  if(id > 13) id = 13;
		  flipView.setSelection(id);
		  
		  String faq = "FAQ " + String.valueOf(id) + "/13";
		  ((TextView)testFlip.this.findViewById(R.id.txt_index)).setText(faq);
		  flipView.setFlipByTouchEnabled(true);
	  }
	  
	  public void onGotoIndex(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
		  
		  flipView.setFlipByTouchEnabled(false);
		  flipView.setSelection(0);
		  
		  ((TextView)testFlip.this.findViewById(R.id.txt_index)).setText("Index");
		  flipView.setFlipByTouchEnabled(true);
	  }
	  
	 @Override
	    protected void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        Session session = Session.getActiveSession();
	        Session.saveSession(session, outState);
	    }
	 
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {
	      super.onActivityResult(requestCode, resultCode, data);
	      Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	  }
	  
	  public void onReturn(View v)
	  {
		  ShareData.Activity_Navigator = true;
		  
		  CSound.sharedSoundController().loadSoundWithName("cartoon.mp3").playForce();
		  
		  this.finish();
		  Intent intent = new Intent(this, SupportFactor.class);
		  startActivity(intent);
	  }
	  
	  public void onShare(View v)
	  {		  
		  if(popuped )
			  return;
		  
		  CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
		  
		  popuped = true;
			  
			linear = (LinearLayout) (findViewById(R.id.document_layout));
			popupView = View.inflate(this, R.layout.share_popup, null);
			popupView.setClickable(true);

			
			popup = new PopupWindow(popupView, Utils.m_screenWidth, Utils.m_screenHeight);
			popup.showAtLocation(linear, Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM, 0, 0);					
			popup.setAnimationStyle(-1);
	  }
	  
	  public void onFacebook(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
		  
			Session session = Session.getActiveSession();
			if(session == null)
			{
				Toast.makeText(this, "Facebook connection failed.", Toast.LENGTH_LONG).show();
				return;
			}
			
			if (!session.isOpened() && !session.isClosed()) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			} else {
				Session.openActiveSession(this, true, statusCallback);
			}
			
			isClicked=true;
			
	  }
	  
		protected void onSessionStateChange(Session session, SessionState state,
				Exception exception) {
			session = Session.getActiveSession();
			if (session.isOpened()) {
				try {
					publishFeedDialog();
				} catch (IOException e) {
//					Log.e(TAG, "Error publish feed");
				}
			} else {
				//nothing .. maybe message
			}

		}
		
		/**
		 * Facebook Post to wall interface
		 * @throws IOException
		 */
		private void publishFeedDialog() throws IOException {

			if(!isClicked) return;
			
			Bundle params = new Bundle();
			params.putString("name", "SweetDream");
			params.putString("caption", "Mattress Number");
			params.putString("description", "Check out your mattress number with Sweetdream App �C My Mattress Number for Android.");
			params.putString("link", "https://itunes.apple.com/us/app/sweetdream-my-mattress-number/id627904788?mt=8&ign-mpt=uo%3D4");
			
			feedDialog = (
					new WebDialog.FeedDialogBuilder(this,
							Session.getActiveSession(),
							params)).setDescription("Check out your mattress number with Sweetdream App �C My Mattress Number for Android.")
							.setOnCompleteListener(new OnCompleteListener() {

								@Override
								public void onComplete(Bundle values,
										FacebookException error) {
									if (error == null) {
										// When the story is posted, echo the success
										// and the post Id.
										final String postId = values.getString("post_id");
										if (postId != null) {


										} else {
											// User clicked the Cancel button
											Toast.makeText(testFlip.this.getApplicationContext(), 
													"Publish cancelled", 
													Toast.LENGTH_SHORT).show();

										}
									} else if (error instanceof FacebookOperationCanceledException) {
										// User clicked the "x" button
										Toast.makeText(testFlip.this.getApplicationContext(), 
												"Publish cancelled", 
	
												Toast.LENGTH_SHORT).show();

									} else {
										// Generic, ex: network error
//										Toast.makeText(getContext().getApplicationContext(), 
//												"Error posting story", 
//												Toast.LENGTH_SHORT).show();

									}
								}

							})
							.build();
			
			isClicked =false;
			
			feedDialog.show();
		}
	  
	  public void onGMail(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
		  
		  String txt = "Check out your mattress number with Sweetdream App - My Mattress Number.";
//		  Intent intent = new Intent(Intent);
//		  intent.setType("text/plain");
//		  intent.putExtra(Intent.EXTRA_TEXT, txt);
//		  intent.putExtra(Intent.EXTRA_EMAIL, "");
//		  startActivity(Intent.createChooser(intent, "Share with GMail"));  
		  
		  Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
		    intent.setType("text/plain");
		    intent.putExtra(Intent.EXTRA_SUBJECT, "Ask Question");
		    intent.putExtra(Intent.EXTRA_TEXT, txt);
		    intent.setData(Uri.parse("mailto:"));// or just "mailto:" for blank
		    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
		    startActivity(Intent.createChooser(intent, "Share with Email"));
	  }
	  
	  public void onCancelShare(View v)
	  {
		  CSound.sharedSoundController().loadSoundWithName("tick.mp3").playForce();
		  
		  if(popup != null && popup.isShowing())
		  {
			  popup.dismiss();
			  popup = null;
			  popuped = false;
		  }
	  }
	  
	  
	  
	  private static class MyBaseAdapter extends BaseAdapter {

		    List<String> urls = new ArrayList<String>();
		    List<WebView> views = new ArrayList<WebView>();
		    
		    ListView listview;
		    
		    int activeLoadingCount = 0;
		    
		    private MyBaseAdapter(Activity activity, FlipViewController controller) {
		    	LayoutInflater inf =  LayoutInflater.from(activity);
		      
		      final Activity act = activity;
		      final FlipViewController cont = controller;
		      
		      listview = (ListView)inf.inflate(R.layout.doc_listview, null);
		      ArrayList<String> array = new ArrayList<String>();
		      array.add("What is BMI?");
		      array.add("What is Support Factor?");
		      array.add("What is mattress Support Factor Index then?");
		      array.add("How can your BMI relates accordingly to the mattress Support Factor Index?");
		      array.add("What is the difference of comfort choice and Support Factor Index? I am confuse.");
		      array.add("How to test the mattress support factor index?");
		      array.add("Does it mean that the higher the support factor index, the higher the comfort?");
		      array.add("Is the Support Factor Index applies to all brand of mattresses?");
		      array.add("My wife and me are having different numbers of Support Factor Index, which one should we choose?");
		      array.add("Can I still use the same mattress if I put on my weight?");
		      array.add("I had purchased the SweetDream mattress with the right Support Factor Index, but why do I still having neck pain issue?");
		      array.add("I had snoring problem, can the SweetDream mattress with Support Factor Index help to stop it?");
		      array.add("Where can I try SweetDream mattresses?");
		      ArrayAdapter<String> adt = new ArrayAdapter<String>(activity, R.layout.linear_txt, array);
		      listview.setAdapter(adt);
		      listview.setBackgroundColor(Color.WHITE);
		      listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					cont.setFlipByTouchEnabled(false);
					cont.setSelection(arg2 + 1);
					
					  String faq = "FAQ " + String.valueOf(arg2 + 1) + "/13";
						((TextView)act.findViewById(R.id.txt_index)).setText(faq);
					
					cont.setFlipByTouchEnabled(true);
						
				}
			});
		      adt.setNotifyOnChange(true);
		      
		      urls.add("file:///android_asset/doc/1.htm");
		      urls.add("file:///android_asset/doc/2.htm");
		      urls.add("file:///android_asset/doc/3.htm");
		      urls.add("file:///android_asset/doc/4.htm");
		      urls.add("file:///android_asset/doc/5.htm");
		      urls.add("file:///android_asset/doc/6.htm");
		      urls.add("file:///android_asset/doc/7.htm");
		      urls.add("file:///android_asset/doc/8.htm");
		      urls.add("file:///android_asset/doc/9.htm");
		      urls.add("file:///android_asset/doc/10.htm");
		      urls.add("file:///android_asset/doc/11.htm");
		      urls.add("file:///android_asset/doc/12.htm");
		      urls.add("file:///android_asset/doc/13.htm");
		      
		      for(int i = 0; i < urls.size(); i++)
		      {
		    	  WebView webView = new WebView(controller.getContext());
			      webView.setWebViewClient(new WebViewClient() {
			        @Override
			        public void onPageStarted(WebView view, String url, Bitmap favicon) {
			        	act.setProgressBarIndeterminateVisibility(true);
			          activeLoadingCount++;
			        }
	
			        @Override
			        public void onPageFinished(WebView view, String url) {
			        	cont.refreshPage(view);
			        	//This works as the webView is the view for a page. Please use refreshPage(int pageIndex) if the webview is only a part of page view.
	
			          activeLoadingCount--;
			          act.setProgressBarIndeterminateVisibility(activeLoadingCount == 0);
			        }
			      });
			      
			      webView.setWebChromeClient(new WebChromeClient() {
			        private int lastRefreshProgress = 0;
	
			        @Override
			        public void onProgressChanged(WebView view, int newProgress) {
			          if (newProgress - lastRefreshProgress
			              > 20) { //limit the invocation frequency of refreshPage
			            cont.refreshPage(view);
			            lastRefreshProgress = newProgress;
			          }
			        }
			      });
		    	  webView.loadUrl(urls.get(i));
		    	  views.add(webView);
		      }		      
		    }
		    
		    @Override
		    public int getCount() {
		      return urls.size() + 1;
		    }

		    @Override
		    public Object getItem(int position) {
		      return position;
		    }

		    @Override
		    public long getItemId(int position) {
		      return position;
		    }

		    @Override
		    public View getView(int position, View convertView, ViewGroup parent) {
//		        View layout = convertView;
//		        if (convertView == null) {
//		    	View layout = inflater.inflate(R.layout.documentation, null);
//		          AphidLog.d("created new view from adapter: %d", position);
//		        }
//		      WebView webView = new WebView(controller.getContext());
//		      webView.setWebViewClient(new WebViewClient() {
//		        @Override
//		        public void onPageStarted(WebView view, String url, Bitmap favicon) {
////		          activity.setProgressBarIndeterminateVisibility(true);
////		          activeLoadingCount++;
//		        }
//
//		        @Override
//		        public void onPageFinished(WebView view, String url) {
////		          controller.refreshPage(
////		              view);//This works as the webView is the view for a page. Please use refreshPage(int pageIndex) if the webview is only a part of page view.
//
////		          activeLoadingCount--;
////		          activity.setProgressBarIndeterminateVisibility(activeLoadingCount == 0);
//		        }
//		      });

//		      webView.setWebChromeClient(new WebChromeClient() {
//		        private int lastRefreshProgress = 0;
//
//		        @Override
//		        public void onProgressChanged(WebView view, int newProgress) {
//		          if (newProgress - lastRefreshProgress
//		              > 20) { //limit the invocation frequency of refreshPage
////		            controller.refreshPage(view);
////		            lastRefreshProgress = newProgress;
//		          }
//		        }
//		      });
		    	
		    	if(position == 0)
		    		return listview;
		    	else
		    	{
				      WebView webView = views.get(position -1);				      
				      return webView;
		    	}

		    }
		  }
	  
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			
			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
			{
				if(popup != null && popup.isShowing())
				{
					popup.dismiss();
					popup = null;
					popuped = false;
					return false;
				}
				
				new AlertDialog.Builder(this).setTitle("SweetDream")
				.setMessage("Do you want to close this app?")
				.setPositiveButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				})
				.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						CSound.sharedSoundController().stopMusic();
						testFlip.this.finish();
					}
				} ).show();
				
				return true;
			}
			
			if (event.getKeyCode() == KeyEvent.KEYCODE_HOME)
			{
				CSound.sharedSoundController().stopMusic();
				return true;
			}
			
			return false;
		}
}
